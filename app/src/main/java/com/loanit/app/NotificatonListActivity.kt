package com.loanit.app

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.loanit.app.adapter.NotificationAdapter
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.NotificationList
import com.loanit.app.net.responsedto.LoanHistoryResponse
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_dashboard_new.*
import kotlinx.android.synthetic.main.layout_toolbar_notification.*
import kotlinx.android.synthetic.main.notifficationlist.*
import org.json.JSONObject

class NotificatonListActivity : AppCompatActivity(), Dispatch {


    private lateinit var linearLayoutManager: LinearLayoutManager

    private lateinit var adapter: NotificationAdapter
    private val listOfNotification: ArrayList<NotificationList> = ArrayList();
    var ListResponse : ArrayList<NotificationList> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notifficationlist)
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Notifications"

        linearLayoutManager = LinearLayoutManager(this)
        notificationList.layoutManager = linearLayoutManager
        adapter = NotificationAdapter(this,listOfNotification)
        notificationList.adapter = adapter

        addData()

        ic_delete.setOnClickListener {
            //startActivity(Intent(this, ProfileActivity::class.java))
            //overridePendingTransition(R.anim.enter, R.anim.exit)
            deleteNotifications();
        }
    }

    public fun deleteNotifications(){

        if(UtilityMethods.isNetworkAvailable(this)){

            APIConnector.deleteNotifications(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {
                    finish();
                    startActivity(getIntent());
                }

                override fun apiError(errorDTO: ErrorDto) {

                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }



    }


    public fun getRefreshData(){

        if(UtilityMethods.isNetworkAvailable(this)){

            APIConnector.getNotificationList(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {
                    //addData()
                    adapter.notifyDataSetChanged()
                }

                override fun apiError(errorDTO: ErrorDto) {

                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }


    public fun addData(){

        if(UtilityMethods.isNetworkAvailable(this)){
            APIConnector.getNotificationList(UserCredentials.getToken(this), this)
        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }



//        for (i in 1..10) {
//            val notificatons = NotificationList()
//           // notificatons.dateTime = "dateeeee";
//            notificatons.title = "Title"+i
//            notificatons.message = "Message****"+i
//            listOfNotification.add(notificatons)
//        }
        adapter.notifyDataSetChanged()
    }


    override fun <T> apiSuccess(body: T) {
       ListResponse = body as ArrayList<NotificationList>
        listOfNotification.addAll(ListResponse);
        adapter.notifyDataSetChanged()
        changeReadStatus();

//        for (no in ListResponse){
//            Log.e("title",no.title);
//            Log.e("message",no.message);
//           // Log.e("title",no.title);
//
//        }
        Log.e("list",body.toString());
        //listOfNotification = ListResponse;

    }

    override fun apiError(error: ErrorDto) {
        Log.e("error",error.toString())

    }

    override fun error(body: String?) {
        Log.e("error1",body.toString())


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }

    fun changeReadStatus(){

        if(UtilityMethods.isNetworkAvailable(this)){
            APIConnector.changeReadStatus(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                }

                override fun apiError(errorDTO: ErrorDto) {

                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }
}