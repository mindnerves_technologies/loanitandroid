package com.loanit.app

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ApplyLoan
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.UserContacts
import com.loanit.app.util.AppConstants
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_apply_loan_new.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import android.content.ActivityNotFoundException
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import com.loanit.app.net.enum.MembershipType
import com.loanit.app.net.responsedto.LoanEmiResponse
import com.loanit.app.util.UtilityView
import kotlinx.android.synthetic.main.activity_apply_loan_new.amount
import kotlinx.android.synthetic.main.activity_apply_loan_new.amount_selected
import kotlinx.android.synthetic.main.activity_apply_loan_new.btn_calculate
import kotlinx.android.synthetic.main.activity_apply_loan_new.charges
import kotlinx.android.synthetic.main.activity_apply_loan_new.checkBox_premium
import kotlinx.android.synthetic.main.activity_apply_loan_new.checkBox_premium_plus
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_amount
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_basic
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_emi_1
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_emi_2
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_heading
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_plus_emi_1
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_plus_emi_2
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_plus_emi_3
import kotlinx.android.synthetic.main.activity_apply_loan_new.lay_premium_plus_heading
import kotlinx.android.synthetic.main.activity_apply_loan_new.sliderAmount
import kotlinx.android.synthetic.main.activity_apply_loan_new.sliderTenure
import kotlinx.android.synthetic.main.activity_apply_loan_new.totalPayable
import kotlinx.android.synthetic.main.activity_apply_loan_new.tvAmountVal
import kotlinx.android.synthetic.main.activity_apply_loan_new.tvTenureVal
import kotlinx.android.synthetic.main.activity_loan_calculator_new.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ApplyLoanActivity : AppCompatActivity(), Dispatch {
    private lateinit var context: Context

    var tenureProgressRecorded: Int = 0
    var penality: Double = 0.0
    var selectedAmount: Int = 0
    var processingFees: Float = 0f
    var isTNCChecked = false
    private var continueProgress: Boolean = false
    private var amountRecorded: Int = 0;
    private var loanAmount: Int = 0;
    var listofAllContacts: ArrayList<UserContacts> = ArrayList()
    var totalEmi: Int = 0
    var subscriptiontype: Int = 0
    var isCalulate: Boolean =false
    var iAgreeTime: String = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply_loan_new)

        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Apply Loan"
        sliderAmount.incrementProgressBy(1000)
        sliderAmount.max = 50000

        //SetDefualt value
        amount.text = "1000"
        amount_selected.text = "1000"
        tvAmountVal.text = "1000"
        amountRecorded = 1000

        var data: String = UserCredentials.getAllContactss(this);
        if (data != null) {
            try {

                val turnsType = object : TypeToken<List<UserContacts>>() {}.type
                listofAllContacts = Gson().fromJson<List<UserContacts>>(data, turnsType) as ArrayList<UserContacts>
            } catch (e: TypeCastException) {
                Log.e("Exception****", "" + e);
            }

        }

        setListner()
        setUI()

    }

    private fun setListner() {
        checkBox_premium.setOnCheckedChangeListener{ _,isChecked ->
            expandPlatinum(isChecked)
            if(isChecked){
                checkBox_premium_plus.isChecked = false
            }
        }

        checkBox_premium_plus.setOnCheckedChangeListener{ _,isChecked ->
            expandPlatinumPlus(isChecked)
            if(isChecked){
                checkBox_premium.isChecked = false
            }
        }

        checkBox.setOnCheckedChangeListener { _, isChecked ->
            isTNCChecked = isChecked
            if (isChecked) {
                var format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
                iAgreeTime = format.format(Date())
                startActivity(Intent(this, WebActivity::class.java)
                        .putExtra("url", AppConstants.TERMS)
                        .putExtra("type", "Term & Conditions"))
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
        }

        btnApplyLoan.setOnClickListener {

            var amountVal = ""
            var tenure = ""
            var processingFee = 0f;

            if(subscriptiontype == 0){
                totalEmi = 0
                amountVal = amount.text.toString()
                tenure = tvTenureVal.text.toString()
                processingFee = processingFees
                isCalulate = true
            }else if(subscriptiontype == 1){
                totalEmi = 2
                amountVal = loanAmount.toString()
                tenure = "62"
            }else if(subscriptiontype == 2){
                if(checkBox_premium.isChecked){
                    totalEmi = 2
                    amountVal = loanAmount.toString()
                    tenure = "62"
                }else if(checkBox_premium_plus.isChecked){
                    totalEmi = 3
                    amountVal = loanAmount.toString()
                    tenure = "93"
                }
            }



            if (isCalulate && amountVal.isNotEmpty() && !amountVal.equals("0") && tenure.isNotEmpty() && !tenure.equals("0")) {

                if (isTNCChecked) {
                    confirmDialog(amountVal, tenure,processingFee,totalEmi)
                } else {
                    UtilityMethods.showToast(this, "Please agree to terms & conditions")
                }

            } else if(!isCalulate){
                UtilityMethods.showToast(this, "Please click calculate to show EMI details")
            } else {
                UtilityMethods.showToast(this, "Please fill details")
            }

        }

        btn_calculate.setOnClickListener{
            if(amountRecorded>0){
                getEMI()
            }else{
                UtilityMethods.showToast(this, "Please select amount")
            }

        }

        sliderAmount.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {

                if (!continueProgress) {
                    seekBar.progress = amountRecorded
                    //  Log.i("You reached max limit", "true")
                } else {
                    //  Log.i("You reached max limit", "else")
                    seekBar.progress = amountRecorded
                }
                charges.text = String.format("%.2f", penality)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                var progresss = progress / 1000;
                var progressss = progresss * 1000;

                // Log.i("progressss limit", "" + progressss)
                if (progressss <= AppConstants.eligibilityAmount!!) {
                    continueProgress = true
                    amountRecorded = progressss

                    var longval = java.lang.Long.parseLong(progressss.toString())
                    val formatter = DecimalFormat("#,##,###")
                    val formattedString = formatter.format(longval)
                    tvAmountVal.text = formattedString
                    amount_selected.text = progressss.toString()
                    amount.text = progressss.toString()

                    if (progressss in 1000..5000) {
                        processingFees = 350f
                    }
                    if (progressss in 6000..15000) {
                        processingFees = 600f
                    }
                    if (progressss > 15000) {
                        processingFees = 1000f
                    }
                    selectedAmount = (amount.text.toString()).toInt()
                    calculate(tenureProgressRecorded)
                    //  charges.text = penality.toString()
                    //charges.text = String.format("%.2f", penality)
                    var total = selectedAmount + penality + processingFees
                    totalPayable.text = total.toString()
                    tvCharges.text = "Processing fee of Rs $processingFees is applicable."
                } else {
                    continueProgress = false
                }
            }
        })
        sliderTenure.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                tvTenureVal.text = seekBar.progress.toString()
                charges.text = String.format("%.2f", penality)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

                tenureProgressRecorded = progress
                calculate(tenureProgressRecorded)
                // charges.text = penality.toString()

                tvTenureVal.text = progress.toString()

                var total = selectedAmount + penality + processingFees
                totalPayable.text = total.toString()

            }
        })
    }

    private fun setUI() {

        if (AppConstants.membershipType == MembershipType.Basic) {
            setBasic()
            subscriptiontype =0; //For Basic

        }
        if (AppConstants.membershipType == MembershipType.Elite) {
            setBasic()
            subscriptiontype =0; //For Basic
        }
        if (AppConstants.membershipType == MembershipType.Standard) {
            setBasic()
            subscriptiontype =0; //For Basic
        }
        if (AppConstants.membershipType == MembershipType.Platinum) {
            setPlatinum()
            subscriptiontype =1; //For Platinum
        }
        if (AppConstants.membershipType == MembershipType.PlatinumPlus) {
            setPlatinumPlus()
            subscriptiontype =2; //For Platinum Plus
        }
    }

    private fun setPlatinumPlus() {
        lay_premium_card.visibility = View.VISIBLE
        lay_premium_plus_card.visibility = View.VISIBLE
        tvServiceCharge.visibility = View.VISIBLE
        lay_amount.visibility = View.VISIBLE
        btn_calculate.visibility = View.VISIBLE
        lay_basic.visibility = View.GONE

        tvCharges.visibility = View.GONE

        checkBox_premium.isChecked = true
        checkBox_premium_plus.isChecked = false

        expandPlatinum(true)
        expandPlatinumPlus(false)

    }

    private fun setPlatinum() {
        lay_premium_card.visibility = View.VISIBLE
        lay_premium_plus_card.visibility = View.VISIBLE
        tvServiceCharge.visibility = View.VISIBLE
        lay_amount.visibility = View.VISIBLE
        btn_calculate.visibility = View.VISIBLE
        lay_basic.visibility = View.GONE

        tvCharges.visibility = View.GONE

        checkBox_premium.isChecked = true
        checkBox_premium_plus.isChecked = false

        expandPlatinum(true)
        expandPlatinumPlus(false)

        checkBox_premium_plus.isEnabled = false
        layPremiumPlus.setBackgroundColor(resources.getColor(R.color.gray))

        checkBox_premium.isEnabled = false
    }

    private fun setPlatinumEmi(res: LoanEmiResponse) {
        tv_premium_emi_1.text = ""+res.platinum_emis.get(0).totalEmiAmount
        premium_emi_1_date.text = ""+res.platinum_emis.get(0).emiDueDate

        tv_premium_emi_2.text = ""+res.platinum_emis.get(1).totalEmiAmount
        premium_emi_2_date.text = ""+res.platinum_emis.get(1).emiDueDate
    }

    private fun setPlatinumPlusEmi(res: LoanEmiResponse) {
        tv_premium_emi_1.text = ""+res.platinum_emis.get(0).totalEmiAmount
        premium_emi_1_date.text = ""+res.platinum_emis.get(0).emiDueDate

        tv_premium_emi_2.text = ""+res.platinum_emis.get(1).totalEmiAmount
        premium_emi_2_date.text = ""+res.platinum_emis.get(1).emiDueDate


        tv_premium_plus_emi_1.text = ""+res.platinum_plus_emis.get(0).totalEmiAmount
        premium_plus_emi_1_date.text = ""+res.platinum_plus_emis.get(0).emiDueDate

        tv_premium_plus_emi_2.text = ""+res.platinum_plus_emis.get(1).totalEmiAmount
        premium_plus_emi_2_date.text = ""+res.platinum_plus_emis.get(1).emiDueDate

        tv_premium_plus_emi_3.text = ""+res.platinum_plus_emis.get(2).totalEmiAmount
        premium_plus_emi_3_date.text = ""+res.platinum_plus_emis.get(2).emiDueDate

    }

    private fun setBasic() {
        lay_premium_card.visibility = View.GONE
        lay_premium_plus_card.visibility = View.GONE
        tvServiceCharge.visibility = View.GONE
        lay_amount.visibility = View.GONE
        btn_calculate.visibility = View.GONE
        lay_basic.visibility = View.VISIBLE
        tvCharges.visibility = View.VISIBLE
    }

    private fun getEMI(){
        if(UtilityMethods.isNetworkAvailable(this)){
            progressLoading.visibility = View.VISIBLE;
            APIConnector.getEMI(UserCredentials.getToken(this), amountRecorded, object : Dispatch{
                override fun <T> apiSuccess(body: T) {

                    progressLoading.visibility = View.GONE;
                    val res = body as LoanEmiResponse
                    if (res.platinum_plus_emis.isNotEmpty()) {
                        setPlatinumPlus();
                        setPlatinumPlusEmi(res)
                        loanAmount = amountRecorded
                        isCalulate = true
                    }else if (res.platinum_emis.isNotEmpty()) {
                        setPlatinum();
                        setPlatinumEmi(res)
                        loanAmount = amountRecorded
                        isCalulate = true
                    } else {
                        UtilityMethods.showToast(applicationContext, "No data found")
                    }

                    Log.e("EMI Responce **********", "" + body.toString());

                }

                override fun apiError(errorDTO: ErrorDto) {

                    progressLoading.visibility = View.GONE;
                    Log.e("Error*********", "" + errorDTO.toString());

                }

                override fun error(body: String?) {
                    progressLoading.visibility = View.GONE;
                    UtilityMethods.showToast(applicationContext, body!!)
                    Log.e("Error*********", "" + body);
                }
            })
        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }
    }



    private fun expandPlatinumPlus(isChecked: Boolean){
        if(isChecked){
            UtilityView.expand(lay_premium_plus_heading);
            UtilityView.expand(lay_premium_plus_emi_1);
            UtilityView.expand(lay_premium_plus_emi_2);
            UtilityView.expand(lay_premium_plus_emi_3);
        }else{
            UtilityView.collapse(lay_premium_plus_heading);
            UtilityView.collapse(lay_premium_plus_emi_1);
            UtilityView.collapse(lay_premium_plus_emi_2);
            UtilityView.collapse(lay_premium_plus_emi_3);
        }
    }

    private fun expandPlatinum(isChecked: Boolean){
        if(isChecked){
            UtilityView.expand(lay_premium_heading);
            UtilityView.expand(lay_premium_emi_1);
            UtilityView.expand(lay_premium_emi_2);
        }else{
            UtilityView.collapse(lay_premium_heading);
            UtilityView.collapse(lay_premium_emi_1);
            UtilityView.collapse(lay_premium_emi_2);
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }

    private fun calculate(progress: Int) {
        if (progress <= 10) {
            penality = 0.0
        }
        if (progress in 11..30) {
            var percentage = (progress - 10) * 0.20
            Log.i("percentage", percentage.toString())
            var amount = selectedAmount * (percentage / 100)
            Log.i("amount", amount.toString())
            penality = (selectedAmount * 2 / 100) + amount
            Log.i("penality", penality.toString())

        }

    }

    private fun congrats() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setTitle("Apply Loan Status")
        dialog.setMessage(resources.getString(R.string.apply_loan))
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->

            if (UserCredentials.isRated(this)) {
                startActivity(Intent(this, DashboardActivity::class.java))
                finish()
            } else {
                rateApp()
            }
        })
        // dialog.setNegativeButton("CLOSE") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun rateApp() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.rateusdialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val yesBtn = dialog.findViewById(R.id.yesBtn) as Button
        val noBtn = dialog.findViewById(R.id.noBtn) as ImageButton
        yesBtn.setOnClickListener {
            dialog.dismiss()
            UserCredentials.setIsRated(this, true)
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.loanit.app")).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + context.packageName)).addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK))
            }
            finish()
        }
        noBtn.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
    }

    private fun confirmDialog(amount: String, tenure: String,processingFee: Float,totalEmis:Int) {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setTitle("Apply Loan Confirmation")
        dialog.setMessage("Are you sure to apply this loan of "+amount+" ?")
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
            if(UtilityMethods.isNetworkAvailable(this)){
                UtilityMethods.showProgressDialog(this)
                val applyLoan = ApplyLoan(amount.toInt(), tenure.toInt(), processingFee,totalEmis,iAgreeTime)
                APIConnector.applyLoan(UserCredentials.getToken(this), applyLoan, this)
            }else{
                UtilityMethods.showToast(this, "Please check your internet connection.")
            }

        })
        dialog.setNegativeButton("Close") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun sendAllContact() {
        if(UtilityMethods.isNetworkAvailable(this)){

            val array = JSONArray()

            for (i in listofAllContacts) {
                val `object` = JSONObject();
                `object`.putOpt("name", i.name);
                `object`.putOpt("mobile", i.mobile);
                array.put(`object`)
            }

            Log.e("Arrrry JSON**********SS", array.toString());
            UtilityMethods.showProgressDialog(this)
            APIConnector.sendAllContacts(UserCredentials.getToken(this), listofAllContacts, object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    Log.e("Fcm Responce **********", "" + body.toString());

                }

                override fun apiError(errorDTO: ErrorDto) {

                    Log.e("Error*********", "" + errorDTO.toString());

                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                    Log.e("Error*********2222", "" + body);
                }
            });

        }else{
            UtilityMethods.showToast(applicationContext, "Please check your internet connection.")
        }


    }

    override fun <T> apiSuccess(body: T) {
        if (listofAllContacts.size != 0) {
            try {

                sendAllContact()
            } catch (e: Exception) {

            }
        }

        congrats()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)
    }

}
