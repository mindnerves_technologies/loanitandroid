package com.loanit.app

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.SeekBar
import android.widget.TextView
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.enum.MembershipType
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.responsedto.LoanEmiResponse
import com.loanit.app.util.AppConstants
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import com.loanit.app.util.UtilityView
import kotlinx.android.synthetic.main.activity_loan_calculator_new.*
import java.text.DecimalFormat

class LoanCalculatorActivity : AppCompatActivity() {

    var penality: Double = 0.0
    var selectedAmount: Int = 0
    var processingFees: Int = 0
    var tenureProgressRecorded: Int = 0
    var amountRecorded: Int = 0
    var context: Context? = null
    var subscriptiontype: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_calculator_new)
        context = this
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Loan Calculator"

        sliderAmount.incrementProgressBy(1000)
        sliderAmount.max = 50000
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //amount.text = "0"
        charges.text = "0"
        totalPayable.text = "0"

        //SetDefualt value
        amount.text = "1000"
        amount_selected.text = "1000"
        tvAmountVal.text = "1000"
        amountRecorded = 1000


        setListner()
        setUI()
    }

    private fun setUI() {

        if (AppConstants.membershipType == MembershipType.Basic) {
            setBasic()
            subscriptiontype =0; //For Basic

        }
        if (AppConstants.membershipType == MembershipType.Elite) {
            setBasic()
            subscriptiontype =0; //For Basic
        }
        if (AppConstants.membershipType == MembershipType.Standard) {
            setBasic()
            subscriptiontype =0; //For Basic
        }
        if (AppConstants.membershipType == MembershipType.Platinum) {
            setPlatinum()
            subscriptiontype =1; //For Platinum
        }
        if (AppConstants.membershipType == MembershipType.PlatinumPlus) {
            setPlatinumPlus()
            subscriptiontype =2; //For Platinum Plus
        }
    }

    private fun setBasic() {
        lay_premium_card_cal.visibility = View.GONE
        lay_premium_plus_card_cal.visibility = View.GONE
        tvServiceCharge_cal.visibility = View.GONE
        lay_amount.visibility = View.GONE
        btn_calculate.visibility = View.GONE
        lay_basic.visibility = View.VISIBLE
    }

    private fun setPlatinum() {
        lay_premium_card_cal.visibility = View.VISIBLE
        lay_premium_plus_card_cal.visibility = View.VISIBLE
        tvServiceCharge_cal.visibility = View.VISIBLE
        lay_amount.visibility = View.VISIBLE
        btn_calculate.visibility = View.VISIBLE
        lay_basic.visibility = View.GONE

        checkBox_premium.isChecked = true
        checkBox_premium_plus.isChecked = false

        expandPlatinum(true)
        expandPlatinumPlus(false)

        checkBox_premium_plus.isEnabled = false
        layPremiumPlus_cal.setBackgroundColor(resources.getColor(R.color.gray))

        checkBox_premium.isEnabled = false
    }

    private fun expandPlatinumPlus(isChecked: Boolean){
        if(isChecked){
            UtilityView.expand(lay_premium_plus_heading);
            UtilityView.expand(lay_premium_plus_emi_1);
            UtilityView.expand(lay_premium_plus_emi_2);
            UtilityView.expand(lay_premium_plus_emi_3);
        }else{
            UtilityView.collapse(lay_premium_plus_heading);
            UtilityView.collapse(lay_premium_plus_emi_1);
            UtilityView.collapse(lay_premium_plus_emi_2);
            UtilityView.collapse(lay_premium_plus_emi_3);
        }
    }

    private fun expandPlatinum(isChecked: Boolean){
        if(isChecked){
            UtilityView.expand(lay_premium_heading);
            UtilityView.expand(lay_premium_emi_1);
            UtilityView.expand(lay_premium_emi_2);
        }else{
            UtilityView.collapse(lay_premium_heading);
            UtilityView.collapse(lay_premium_emi_1);
            UtilityView.collapse(lay_premium_emi_2);
        }
    }

    private fun setPlatinumPlus() {
        lay_premium_card_cal.visibility = View.VISIBLE
        lay_premium_plus_card_cal.visibility = View.VISIBLE
        tvServiceCharge_cal.visibility = View.VISIBLE
        lay_amount.visibility = View.VISIBLE
        btn_calculate.visibility = View.VISIBLE
        lay_basic.visibility = View.GONE

        checkBox_premium.isChecked = true
        checkBox_premium_plus.isChecked = false

        expandPlatinum(true)
        expandPlatinumPlus(false)

    }


    private fun setListner() {
        sliderAmount.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {

                charges.text = String.format("%.2f", penality)

            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {


                var progresss = progress / 1000;
                var progressss = progresss * 1000;

                amountRecorded = progressss

                var longval = java.lang.Long.parseLong(progressss.toString())
                val formatter = DecimalFormat("#,##,###")
                val formattedString = formatter.format(longval)
                tvAmountVal.text = formattedString
                amount_selected.text = formattedString

                amount .text = progressss.toString()

                if (progressss in 1000..5000) {
                    processingFees = 350
                }
                if (progressss in 6000..15000) {
                    processingFees = 600
                }
                if (progressss > 15000) {
                    processingFees = 1000
                }
                selectedAmount = (amount.text.toString()).toInt()

                calculate(tenureProgressRecorded)
                //charges.text = penality.toString()
                var total = selectedAmount + penality + processingFees
                totalPayable.text = total.toString()
                tvFAQ.text = "Processing fee of Rs $processingFees is applicable. For more details read FAQS."
            }
        })
        sliderTenure.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                tvTenureVal.text = seekBar.progress.toString()
                charges.text = String.format("%.2f", penality)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {

                tenureProgressRecorded = progress
                calculate(tenureProgressRecorded)
                //charges.text = penality.toString()
                var total = selectedAmount + penality + processingFees
                totalPayable.text = total.toString()
                tvTenureVal.text = progress.toString()
            }
        })

        tvFAQ.setOnClickListener {
            startActivity(Intent(context, WebActivity::class.java).putExtra("url", AppConstants.FAQ))
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        checkBox_premium.setOnCheckedChangeListener{ _,isChecked ->
            expandPlatinum(isChecked)
            if(isChecked){
                checkBox_premium_plus.isChecked = false
            }
        }

        checkBox_premium_plus.setOnCheckedChangeListener{ _,isChecked ->
            expandPlatinumPlus(isChecked)
            if(isChecked){
                checkBox_premium.isChecked = false
            }
        }

        btn_calculate.setOnClickListener{
            if(amountRecorded>0){
                getEMI()
            }else{
                UtilityMethods.showToast(this, "Please select amount")
            }
        }
    }


    private fun getEMI(){
        if(UtilityMethods.isNetworkAvailable(this)){

            progressLoadingCal.visibility = View.VISIBLE;
            APIConnector.getEMI(UserCredentials.getToken(this), amountRecorded, object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    progressLoadingCal.visibility = View.GONE;
                    val res = body as LoanEmiResponse
                    if (res.platinum_plus_emis.isNotEmpty()) {
                        setPlatinumPlus();
                        setPlatinumPlusEmi(res)
                    }else if (res.platinum_emis.isNotEmpty()) {
                        setPlatinum();
                        setPlatinumEmi(res)
                    } else {
                        UtilityMethods.showToast(applicationContext, "No data found")
                    }
                    Log.e("EMI Responce **********", "" + body.toString());

                }

                override fun apiError(errorDTO: ErrorDto) {

                    progressLoadingCal.visibility = View.GONE;
                    Log.e("Error*********", "" + errorDTO.toString());

                }

                override fun error(body: String?) {
                    progressLoadingCal.visibility = View.GONE;
                    UtilityMethods.showToast(applicationContext, body!!)
                    Log.e("Error*********", "" + body);
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }

    private fun setPlatinumEmi(res: LoanEmiResponse) {
        tv_premium_emi_1_cal.text = ""+res.platinum_emis.get(0).totalEmiAmount
        premium_emi_1_date_cal.text = ""+res.platinum_emis.get(0).emiDueDate

        tv_premium_emi_2_cal.text = ""+res.platinum_emis.get(1).totalEmiAmount
        premium_emi_2_date_cal.text = ""+res.platinum_emis.get(1).emiDueDate
    }
    private fun setPlatinumPlusEmi(res: LoanEmiResponse) {
        tv_premium_emi_1_cal.text = ""+res.platinum_emis.get(0).totalEmiAmount
        premium_emi_1_date_cal.text = ""+res.platinum_emis.get(0).emiDueDate

        tv_premium_emi_2_cal.text = ""+res.platinum_emis.get(1).totalEmiAmount
        premium_emi_2_date_cal.text = ""+res.platinum_emis.get(1).emiDueDate


        tv_premium_plus_emi_1_cal.text = ""+res.platinum_plus_emis.get(0).totalEmiAmount
        premium_plus_emi_1_date_cal.text = ""+res.platinum_plus_emis.get(0).emiDueDate

        tv_premium_plus_emi_2_cal.text = ""+res.platinum_plus_emis.get(1).totalEmiAmount
        premium_plus_emi_2_date_cal.text = ""+res.platinum_plus_emis.get(1).emiDueDate

        tv_premium_plus_emi_3_cal.text = ""+res.platinum_plus_emis.get(2).totalEmiAmount
        premium_plus_emi_3_date_cal.text = ""+res.platinum_plus_emis.get(2).emiDueDate

    }

    private fun calculate(progress: Int) {

        if (progress <= 10) {
            penality = 0.0
        }
        if (progress in 11..30) {
            var percentage = (progress - 10) * 0.20
            var amount = selectedAmount * percentage / 100
            penality = (selectedAmount * 2 / 100) + amount


        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }

    //resmyl.com
    //resmile.co
    //smilepe.com
    //smilese.org

    //smilepe pe jayiye aur
    //chinta ki kya bat jb ho resimle ka sath
    //mat ho dukhi use rsyml and loa chehre pe khushi
//    rakhe apki sehat ka kyal aur laye chehre pe muskrahat"
}
