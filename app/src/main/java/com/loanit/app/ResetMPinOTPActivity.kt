package com.loanit.app

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.ResetMpinInit
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_reset_mpin_otp.*

class ResetMPinOTPActivity : AppCompatActivity(), Dispatch {


    private var mobileNumber: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_mpin_otp)

        btnGenerateResetKey.setOnClickListener {
            val mobileVal = etMobile.text.toString();
            mobileNumber = mobileVal
            if (mobileVal.length == 10) {

                if(UtilityMethods.isNetworkAvailable(this)){
                    val resetMpinInit = ResetMpinInit(mobileVal)
                    UtilityMethods.showProgressDialog(this)
                    APIConnector.resetMPinInit(UserCredentials.getToken(this), resetMpinInit, this)
                }else{
                    UtilityMethods.showToast(this, "Please check your internet connection.")
                }

            } else {
                UtilityMethods.showToast(this, "Enter 10 digit mobile number")
            }
        }
    }


    override fun <T> apiSuccess(body: T) {
        startActivity(Intent(this, ResetMPinActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
    }
}
