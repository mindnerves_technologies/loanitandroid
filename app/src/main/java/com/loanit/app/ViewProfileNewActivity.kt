package com.loanit.app

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods


class ViewProfileNewActivity : AppCompatActivity(), Dispatch {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile_new)
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Your Profile"

        if(UtilityMethods.isNetworkAvailable(this)){
            APIConnector.getProfile(UserCredentials.getToken(this), this)
        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }


    }

    override fun <T> apiSuccess(body: T) {
      //  val getProfile = body as GetProfile

//        etAadharNumber.setText(getProfile.aadharNumber)
//        etAccountNo.setText(getProfile.accountNumber)
//        etAlternateEmail.setText(getProfile.alternateEmail)
//        etMobile.setText(getProfile.alternateMobile)
//        etIfsc.setText(getProfile.ifscCode)
//        officeEmail.setText(getProfile.officeEmail)
//        officeNumber.setText(getProfile.officeNumber)
//        degreeVal.text = getProfile.degree
//        passOutVal.text = getProfile.passoutYear
//        monthlyRentVal.text = getProfile.rentType?.name
//        holidayVal.text = getProfile.holidayType?.name
//        travelVal.text = getProfile.travelType?.name
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }

}
