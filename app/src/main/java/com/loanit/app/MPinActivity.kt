package com.loanit.app

import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_mpin.*

class MPinActivity : AppCompatActivity(), Dispatch {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mpin)

        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        if(intent.getBooleanExtra("textChange",false)){
            etconfirmMPin.visibility = View.GONE
            header2.visibility = View.GONE
            btnMPin.text = "SUBMIT"
            header.text = "ENTER MPIN TO LOGIN"
        }

      //  supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "LoanIt"

        btnMPin.setOnClickListener {

            val mPinVal = etMPin.text.toString();
            val confirmPinVal = etconfirmMPin.text.toString()

            if (mPinVal.isNotEmpty() && confirmPinVal.isNotEmpty()) {

                if (mPinVal == confirmPinVal) {

                    if (mPinVal.length == 4) {
                        UtilityMethods.showProgressDialog(this)
                        if(UtilityMethods.isNetworkAvailable(this)){
                            APIConnector.setMPin(UserCredentials.getToken(this), mPinVal, this)
                        }else{
                            UtilityMethods.showToast(this, "Please check your internet connection.")
                        }

                    } else {
                        UtilityMethods.showToast(this, "MPIN must be of 4 digits");
                    }

                } else {
                    UtilityMethods.showToast(this, "MPIN not matched");
                }
            } else {
                UtilityMethods.showToast(this, "Enter Valid MPIN");
            }
        }
    }

    override fun <T> apiSuccess(body: T) {

        confirmDialog()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(this, body!!)
    }


    private fun confirmDialog() {



        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setMessage(resources.getString(R.string.mpin_set_message))
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
            startActivity(Intent(this, DashboardActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()
        })
       // dialog.setNegativeButton("CLOSE") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }



}
