package com.loanit.app.util

import com.google.gson.Gson
import com.loanit.app.net.enum.MembershipType


class AppConstants {

        companion object {
        //const val BASE_URL = "http://l1-env.azawbjerch.us-east-1.elasticbeanstalk.com/api/v1/"  //prod
        //const val BASE_URL = "http://192.168.43.28:8091/api/v1/"    //local
        const val BASE_URL = "http://52.203.129.88:8091/api/v1/" //dev
        var gson = Gson()
        var nameOfUser = ""
        var mobileOfUser = ""
        var eligibilityAmount: Int? = 0
        var membershipType: MembershipType? = null
        const val FAQ = "https://s3.amazonaws.com/loanit.images/faq.html"
        const val TERMS = "https://s3.amazonaws.com/loanit.images/terms_to_use.html"
        const val PRIVACY = "https://s3.amazonaws.com/loanit.images/terms_to_use.html"
    }
}


//Keystore File(.jks)
// Keystore file credentials