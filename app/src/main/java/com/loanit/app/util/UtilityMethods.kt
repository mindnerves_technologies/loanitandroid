package com.loanit.app.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.loanit.app.R

/**
 * Created by sahil on 15/9/17.
 */

class UtilityMethods {


    companion object {

        var dialog_changed: Dialog? = null

        fun isNetworkAvailable(context: Context): Boolean {
            try {
                val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                return activeNetworkInfo != null && activeNetworkInfo.isConnected
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return false
        }


        fun showToast(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }


        fun showProgressDialog(context: Context) {
            if (dialog_changed != null) {
                dialog_changed!!.dismiss()
            }
            dialog_changed = Dialog(context)
            dialog_changed!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog_changed!!.setContentView(R.layout.progressdialog)
            dialog_changed!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog_changed!!.setCancelable(false)
            dialog_changed!!.show()

        }

        fun dismissProgressDialog() {
            try {
                if (dialog_changed != null) {
                    dialog_changed!!.dismiss()
                }
            } catch (e: Exception) {
                Log.e("progress problem", "yes")
            }
        }
    }
}
