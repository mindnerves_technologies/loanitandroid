package com.loanit.app.util

import android.content.Context
import android.content.SharedPreferences
import android.provider.Telephony
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.loanit.app.net.requestdto.UserContacts
import java.util.ArrayList

/**
 * Created by hocrox_java on 08/05/18.
 */
class UserCredentials {

    companion object {
        private const val TOKEN_VALUE = "Bearer"
        private const val TOKEN = "token"
        private const val NAME = "name"
        private const val EMAIL = "email"
        private const val MOBILE = "mobile"
        private const val FCMTOKEN="fcmtoken"
        private const val CONTACTSS="contact"
        private const val RATED="rated"


        fun setToken(context: Context, token: String) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putString(TOKEN, "$TOKEN_VALUE $token")
            editor.apply()
        }

        fun getToken(context: Context): String = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE).getString(TOKEN, "")


        fun setName(context: Context, name: String) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putString(NAME, name)
            editor.apply()
        }

        fun getName(context: Context): String = context.getSharedPreferences(NAME, Context.MODE_PRIVATE).getString(NAME, "")


        fun setEmail(context: Context, name: String) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(EMAIL, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putString(EMAIL, name)
            editor.apply()
        }

        fun getEmail(context: Context): String = context.getSharedPreferences(EMAIL, Context.MODE_PRIVATE).getString(EMAIL, "")


        fun setMobile(context: Context, mobile: String) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(MOBILE, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putString(MOBILE, mobile)
            editor.apply()
        }

        fun getMobile(context: Context): String = context.getSharedPreferences(MOBILE, Context.MODE_PRIVATE).getString(MOBILE, "")


        fun setFcmToken(context: Context,token:String){

            val sharedPreference: SharedPreferences = context.getSharedPreferences(FCMTOKEN, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putString(FCMTOKEN, token)
            editor.apply()

        }


        fun getFcmToken(context: Context):String=context.getSharedPreferences(FCMTOKEN,Context.MODE_PRIVATE).getString(FCMTOKEN,"");


        fun saveContactlist(context: Context,listOFDirectories: ArrayList<UserContacts>) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(CONTACTSS, Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            val gson = Gson()
            val json = gson.toJson(listOFDirectories)
            editor.putString(CONTACTSS, json)
            editor.apply()
        }


        fun getAllContactss(context: Context):String=context.getSharedPreferences(CONTACTSS,Context.MODE_PRIVATE).getString(CONTACTSS,"");

       /* fun getSavedContacts(context: Context): ArrayList<UserContacts> {
            var listofSavedDirectories = ArrayList<UserContacts>()

            val gson = Gson()
           // Log.e("gsonKey*******", "" + prefs.getString(USER_DIRECTORY_LIST, "")!!)

            val typeContact = object : TypeToken<List<UserContacts>>() {

            }.type
            if (context.getSharedPreferences(CONTACTSS,Context.MODE_PRIVATE).getString(CONTACTSS, "")!=null) {
                listofSavedDirectories = gson.fromJson<ArrayList<UserContacts>>(context.getSharedPreferences(CONTACTSS, Context.MODE_PRIVATE).getString(CONTACTSS, ""), typeContact)
            }

            return listofSavedDirectories
        }*/



        fun setIsRated(context: Context, isRate: Boolean) {
            val sharedPreference: SharedPreferences = context.getSharedPreferences(RATED,
                    Context.MODE_PRIVATE)
            var editor: SharedPreferences.Editor = sharedPreference.edit()
            editor.putBoolean(RATED, isRate)
            editor.apply()
        }

        fun isRated(context: Context): Boolean = context.getSharedPreferences(RATED,
                Context.MODE_PRIVATE).getBoolean(RATED, false)

    }
}