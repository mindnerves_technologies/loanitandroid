package com.loanit.app.util;

/**
 * Created by hocrox_java on 15/12/18.
 */

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

/** Metadata for a user selection (but not yet upload) of a local or cloud file. */
public class Selection implements Parcelable {
    public static final Parcelable.Creator<Selection> CREATOR = new Creator();

//    private String provider;
    private String path;
    private Uri uri;
    private int size;
    private String mimeType;
    private String name;

    public Selection(Uri uri, int size, String mimeType, String name) {
        this.uri = uri;
        this.size = size;
        this.mimeType = mimeType;
        this.name = name;
        this.path = uri.getPath();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Selection selection = (Selection) o;

        if (size != selection.size) return false;
        if (path != null ? !path.equals(selection.path) : selection.path != null) return false;
        if (uri != null ? !uri.equals(selection.uri) : selection.uri != null) return false;
        if (mimeType != null ? !mimeType.equals(selection.mimeType) : selection.mimeType != null)
            return false;
        return name != null ? name.equals(selection.name) : selection.name == null;
    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (uri != null ? uri.hashCode() : 0);
        result = 31 * result + size;
        result = 31 * result + (mimeType != null ? mimeType.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    /** The path of this file in a cloud provider. Null if a local file. */
    public String getPath() {
        return path;
    }

    /** The Android system URI of this file. Null if a cloud file. */
    public Uri getUri() {
        return uri;
    }

    /** Size in bytes. */
    public int getSize() {
        return size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getName() {
        return name;
    }

    private static class Creator implements Parcelable.Creator<Selection> {
        @Override
        public Selection createFromParcel(Parcel in) {
            return new Selection(in);
        }

        @Override
        public Selection[] newArray(int size) {
            return new Selection[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(path);
        out.writeParcelable(uri, flags);
        out.writeInt(size);
        out.writeString(mimeType);
        out.writeString(name);
    }

    private Selection(Parcel in) {
        path = in.readString();
        uri = in.readParcelable(Uri.class.getClassLoader());
        size = in.readInt();
        mimeType = in.readString();
        name = in.readString();
    }

    public String toString() {
        return "Path : " + path
                + "\n URI : " + uri
                + "\n Size : " + size
                + "\n Mime : " + mimeType
                + "\n Name : " + name;
    }
}