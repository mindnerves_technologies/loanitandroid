package com.loanit.app

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.loanit.app.adapter.LoanStatusAdapter
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.responsedto.LoanStatusResponse
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_loan_status.*

/**
 * Created by hocrox_java on 20/11/18.
 */
class LoanStatusActivity : AppCompatActivity(), Dispatch {


    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan_status)

        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Loan Status"
        //UtilityMethods.showProgressDialog(this)

        if(UtilityMethods.isNetworkAvailable(this)){
            APIConnector.activeLoansEmi(UserCredentials.getToken(this), this)
        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }


    override fun <T> apiSuccess(body: T) {

        progressLoading.visibility = View.GONE
        var newsListResponse = body as List<LoanStatusResponse>

        if (newsListResponse.isNotEmpty()) {
            linearLayoutManager = LinearLayoutManager(this)
            loanHistory.layoutManager = linearLayoutManager

            val loanHistoryAdapter = LoanStatusAdapter(this, newsListResponse)
            loanHistory.adapter = loanHistoryAdapter
        } else {
            UtilityMethods.showToast(this, "No Loan Found")
        }
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }


}