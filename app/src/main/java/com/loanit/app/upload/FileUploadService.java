package com.loanit.app.upload;

import com.loanit.app.net.responsedto.UploadImageModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by hocrox_java on 31/08/16.
 */
public interface FileUploadService {

    @Multipart
    @POST("completeProfile/uploadDocument")
    Call<UploadImageModel> uploadProfilePic(@Header("Authorization") String token, @Part MultipartBody.Part file);


}

