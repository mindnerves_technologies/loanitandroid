package com.loanit.app

import android.Manifest
import android.content.ContentResolver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.Gson
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.enum.MembershipType
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.FcmToken
import com.loanit.app.net.requestdto.UserContacts
import com.loanit.app.net.responsedto.LoanHistoryResponse
import com.loanit.app.net.responsedto.UserEligibilityDetails
import com.loanit.app.util.AppConstants
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_dashboard_new.*
import org.json.JSONArray
import lombok.experimental.`var`
import com.google.gson.reflect.TypeToken
import com.loanit.app.util.AppConstants.Companion.gson
import org.json.JSONObject


class DashboardActivity : AppCompatActivity(), Dispatch, NavigationView.OnNavigationItemSelectedListener {

    private lateinit var context: Context
    var applyLoan = false

    var listofAllContacts : ArrayList<UserContacts> = ArrayList()
    var listofAllContactsSession : ArrayList<UserContacts> = ArrayList()

    companion object {
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_new)
        context = this
        //UtilityMethods.showProgressDialog(this)
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.mipmap.nav_icon)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener(this)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        val notifications = findViewById<View>(R.id.notifications) as ImageView
        toolbarText.text = "LoanIt"
        //APIConnector.getEligibilityForm(UserCredentials.getToken(this), this)
        //APIConnector.getUnReadCount(UserCredentials.getToken(this), this)

        completeProfile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)

        }
        notifications.setOnClickListener {
            startActivity(Intent(this, NotificatonListActivity::class.java))
            //startActivity(Intent(context, NotificationsActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
        tvFaq.setOnClickListener {
            startActivity(Intent(context, WebActivity::class.java).
                    putExtra("url", AppConstants.FAQ)
                    .putExtra("type", "FAQs")
            )
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }
        loanCalculatorRel.setOnClickListener {
            startActivity(Intent(this, LoanCalculatorActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)

        }
        loanStatusRel.setOnClickListener {
            startActivity(Intent(this, LoanStatusActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)

        }
        applyLoanRel.setOnClickListener {
            startActivity(Intent(this, ApplyLoanActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)

        }

        termsBtn.setOnClickListener {
            startActivity(Intent(context, WebActivity::class.java)
                    .putExtra("url", AppConstants.TERMS)
                    .putExtra("type", "Term & Conditions")
            )
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        repayRel.setOnClickListener {
            startActivity(Intent(this, RepayActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)
        }

        //loadContacts();

       /* listofAllContactsSession=UserCredentials.getSavedContacts(this)*/
        val data:String?=UserCredentials.getAllContactss(this);
        if (data!=null  && data.length > 0){

            val turnsType = object : TypeToken<List<UserContacts>>() {}.type
            listofAllContactsSession = Gson().fromJson<List<UserContacts>>(data, turnsType) as ArrayList<UserContacts>

        }
       /* if (listofAllContactsSession.size>0) {
            for (p in listofAllContactsSession) {
                Log.e("Conatcts FromSession********", "" + p.name)
            }
        }*/

        if (listofAllContactsSession.size==0) {
            getContactTask(this).execute()
        }

       /* for (i in listofAllContacts){
            Log.e("ContactNameeeeee",i.userName);
        }*/

    }


    override fun <T> apiSuccess(body: T) {

        val userEligibilityDetails = body as UserEligibilityDetails

        tvName.text = "Welcome " + userEligibilityDetails.name
        tvEligibilityAmount.text = "" + userEligibilityDetails.eligibilityAmount
        tvLoanCredit.text = "" + userEligibilityDetails.totalCredit

        Log.i("ENUM", userEligibilityDetails.membershipType?.name)
        if (userEligibilityDetails.membershipType == MembershipType.Basic) {
            tvMembershipType.text = "BASIC"
        }
        if (userEligibilityDetails.membershipType == MembershipType.Elite) {
            tvMembershipType.text = "ELITE"
        }
        if (userEligibilityDetails.membershipType == MembershipType.Standard) {
            tvMembershipType.text = "STANDARD"
        }
        if (userEligibilityDetails.membershipType == MembershipType.Platinum) {
            tvMembershipType.text = "PLATINUM"
        }
        if (userEligibilityDetails.membershipType == MembershipType.PlatinumPlus) {
            tvMembershipType.text = "PLATINUM +"
        }

        AppConstants.eligibilityAmount = userEligibilityDetails.eligibilityAmount
        AppConstants.nameOfUser = userEligibilityDetails.name!!
        AppConstants.mobileOfUser = userEligibilityDetails.contact!!
        AppConstants.membershipType = userEligibilityDetails.membershipType

        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        val header = navigationView.getHeaderView(0)
        val tvName = header.findViewById(R.id.tvName) as TextView
        val badge_notification = findViewById(R.id.badge_notification) as TextView

        val tvMobile = header.findViewById(R.id.tvMobile) as TextView
        tvName.text = AppConstants.nameOfUser
        tvMobile.text = AppConstants.mobileOfUser
        //badge_notification.text="20";
        curveLayout.visibility = View.VISIBLE
        progressLoading.visibility = View.GONE
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)
    }


    override fun onStart() {
        // TODO Auto-generated method stub
        super.onStart()
        getUnReadCount();
    }

    override fun onResume() {
        super.onResume()
        if(UtilityMethods.isNetworkAvailable(this)){
            APIConnector.getProfileAnother(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    applyLoanRel.visibility = View.VISIBLE
                    completeProfile.visibility = View.GONE
                    applyLoan = true

                    scroll.visibility = View.VISIBLE
                    progressLoading.visibility = View.GONE

                }

                override fun apiError(errorDTO: ErrorDto) {
                    if (errorDTO.description == "User Profile Not Found")
                        applyLoanRel.visibility = View.GONE
                    completeProfile.visibility = View.VISIBLE
                    applyLoan = false
                    scroll.visibility = View.VISIBLE
                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

            APIConnector.getEligibilityForm(UserCredentials.getToken(this), this)

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
                drawer.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.profile -> {
                startActivity(Intent(context, ViewProfileActivity::class.java))
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            R.id.loanCalculator -> {
                startActivity(Intent(context, LoanCalculatorActivity::class.java))
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            R.id.loanHistory -> {
                startActivity(Intent(context, LoanHistoryActivity::class.java))
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            R.id.faqs -> {
                startActivity(Intent(context, WebActivity::class.java).
                        putExtra("url", AppConstants.FAQ)
                        .putExtra("type", "FAQs")
                )
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }
            R.id.applyLoan -> {
                if (applyLoan) {
                    startActivity(Intent(context, ApplyLoanActivity::class.java))
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                } else {
                    confirmDialog()
                }
            }
            R.id.contact -> {
                startActivity(Intent(context, ContactUsActivity::class.java))
                overridePendingTransition(R.anim.enter, R.anim.exit)
            }

            R.id.logout -> {
                UserCredentials.setToken(this, "")
                UtilityMethods.showToast(this, "User Logged Out...")
                startActivity(Intent(context, LoginActivity::class.java))
                overridePendingTransition(R.anim.enter, R.anim.exit)
                finishAffinity()
            }
        }
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }


    private fun confirmDialog() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setMessage(resources.getString(R.string.apply_message))
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
        })
        dialog.setCancelable(true)
        dialog.show()
    }

    fun getUnReadCount(){
        if(UtilityMethods.isNetworkAvailable(this)){

            APIConnector.getUnReadCount(UserCredentials.getToken(this), object : Dispatch {
                override fun <Int> apiSuccess(body: Int) {
                    val badge_notification = findViewById(R.id.badge_notification) as TextView
                    if(body.toString()=="0"){
                        badge_notification.visibility = View.GONE;
                    }else if(body.toString().length==1){
                        badge_notification.text= "0"+body.toString();
                        badge_notification.visibility = View.VISIBLE;
                    }else{
                        badge_notification.text= body.toString();
                        badge_notification.visibility = View.VISIBLE;
                    }


                }

                override fun apiError(errorDTO: ErrorDto) {

                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }
    }



    private fun loadContacts() {
        var builder = StringBuilder()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                        Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS),
                    PERMISSIONS_REQUEST_READ_CONTACTS)
            //callback onRequestPermissionsResult
        } else {
            listofAllContacts = getContacts()
            //listContacts.text = builder.toString()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()
            } else {
                //  toast("Permission must be granted in order to display contacts information")
            }
        }
    }





    private fun getContacts():  ArrayList<UserContacts> {
        var im:Int=0;
        val builder = StringBuilder()
        val resolver: ContentResolver = contentResolver;
        var listContacts : ArrayList<UserContacts> = ArrayList()
        val cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null,
                null)

        if (cursor.count > 0) {
            while (cursor.moveToNext()) {
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                val phoneNumber = (cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phoneNumber > 0) {
                    val cursorPhone = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)

                    if(cursorPhone.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            im++;
                            val phoneNumValue = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            builder.append("Contact: ").append(name).append(", Phone Number: ").append(
                                    phoneNumValue).append("\n\n")
                           // Log.e("Name ===>",name+"Number====>"+  phoneNumValue);
                            //Log.e("Contactssss**Build**",""+builder.toString());




                                val userContact = UserContacts(name, phoneNumValue);
                                listContacts.add(userContact);


                        }
                    }
                    cursorPhone.close()
                }
            }
        } else {
            //   toast("No contacts available!")
        }
        cursor.close()
        return listContacts
    }



    class getContactTask(private val activity: DashboardActivity) : AsyncTask<Void, Void, String>() {

        var innerActivity: DashboardActivity = activity
        override fun doInBackground(vararg params: Void?): String? {
            innerActivity.loadContacts();
            // ...
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            // ...
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            // ...

            for (i in innerActivity.listofAllContacts){
                Log.e("ContactNameeeeee",i.name);

            }

            UserCredentials.saveContactlist(innerActivity,innerActivity.listofAllContacts)

         //   innerActivity.sendAllContact()


        }
    }


    private fun sendAllContact(){
        val array = JSONArray()

        for (i in listofAllContacts){
            if(i.equals(50)){
                break;
            }
            val `object` = JSONObject();
            `object`.putOpt("name",i.name);
            `object`.putOpt("mobile", i.mobile);
            array.put(`object`)
        }

        Log.e("Arrrry JSON**********SS",array.toString());
        UtilityMethods.showProgressDialog(context)
        APIConnector.sendAllContacts(UserCredentials.getToken(this),listofAllContacts, object : Dispatch {
            override fun <T> apiSuccess(body: T) {

                Log.e("Fcm Responce **********",""+body.toString());

            }

            override fun apiError(errorDTO: ErrorDto) {

                Log.e("Error*********",""+errorDTO.toString());

            }

            override fun error(body: String?) {
                UtilityMethods.showToast(applicationContext, body!!)
                Log.e("Error*********2222",""+body);
            }
        });

    }



}
