package com.loanit.app

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.ResetMpinFinish
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_reset_mpin.*

class ResetMPinActivity : AppCompatActivity(), Dispatch {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_mpin)
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "LoanIt"

        btnMPin.setOnClickListener {
            if(UtilityMethods.isNetworkAvailable(this)){

                val resetVal = etResetKey.text.toString()
                val newMpinVal = etNewMpin.text.toString()
                if (resetVal.isNotEmpty() && newMpinVal.isNotEmpty()) {
                    if (newMpinVal.length == 4) {
                        UtilityMethods.showProgressDialog(this)
                        val resetMpinFinish = ResetMpinFinish(resetVal, newMpinVal)
                        APIConnector.resetMPinFinish(UserCredentials.getToken(this), resetMpinFinish, this)
                    } else {
                        UtilityMethods.showToast(this, "MPIN must be of 4 digits");
                    }
                } else {
                    UtilityMethods.showToast(this, "Enter Valid MPIN");
                }

            }else{
                UtilityMethods.showToast(this, "Please check your internet connection.")
            }

        }
    }

    override fun <T> apiSuccess(body: T) {

        startActivity(Intent(this, MPinAgainActivity::class.java).putExtra("textChange", true))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(this, body!!)
    }
}



