package com.loanit.app

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_web.*


class WebActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView

        when (intent.getStringExtra("type")) {

            "FAQs" -> toolbarText.text = "FAQs"
            "Term & Conditions" -> toolbarText.text = "Terms & Conditions"
        }
        UtilityMethods.showProgressDialog(this)
        val handler = Handler()
        handler.postDelayed(Runnable {
            UtilityMethods.dismissProgressDialog()
        }, 2500L)

        web.settings.javaScriptEnabled = true;
        web.settings.defaultTextEncodingName = "utf-8";

        web.webViewClient = MyBrowser()
        var urlToLoad = intent.getStringExtra("url")
        web.loadUrl(urlToLoad);

    }

    private inner class MyBrowser : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }
}
