package com.loanit.app

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.enum.LoginType
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.FcmToken
import com.loanit.app.net.requestdto.LoginDto
import com.loanit.app.net.responsedto.GetUser
import com.loanit.app.net.responsedto.LoginResponse
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false
    private var callBackManager: CallbackManager? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private lateinit var context: Context
    private lateinit var socialUserId: String
    private var socialLoginFirstName: String? = null
    private var socialLoginLastName: String? = null
    private var socialLoginName: String? = null
    private var socialLoginEmail: String? = null
    private val RC_SIGN_IN: Int = 300

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        context = this
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(resources.getString(R.string.google_sigin))
                .requestServerAuthCode(resources.getString(R.string.google_sigin))
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        callBackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(callBackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        socialUserId = AccessToken.getCurrentAccessToken().userId
                        Log.i("socialUserId", socialUserId);
                        val facebookPassword = AccessToken.getCurrentAccessToken().token
                        Log.i("facebookPassword", facebookPassword)
                        Log.i("Access Token", loginResult.accessToken.token)

                        val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
                            Log.i("LoginActivity", response.toString())
                            val bFacebookData = getFacebookData(`object`)
                            Log.i("Fb_email", bFacebookData?.get("email").toString());
                            socialLoginEmail = bFacebookData?.get("email").toString()
                            socialLoginFirstName = bFacebookData!!.get("first_name")!!.toString()
                            socialLoginLastName = bFacebookData.get("last_name")!!.toString()

                            socialLoginName = "$socialLoginFirstName $socialLoginLastName"

                            Log.i("FB_first_name", bFacebookData.get("first_name")!!.toString())
                            socialLogin(socialUserId, facebookPassword, LoginType.FACEBOOK)
                        }
                        val parameters = Bundle()
                        parameters.putString("fields", "id, first_name, last_name, email")
                        request.parameters = parameters
                        request.executeAsync()
                    }

                    override fun onCancel() {
                        UtilityMethods.showToast(baseContext, "Try again")
                    }

                    override fun onError(exception: FacebookException) {
                        UtilityMethods.showToast(baseContext, exception.cause.toString())
                    }
                })


        btnFb.setOnClickListener {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile, email"))
        }

        btnGoogle.setOnClickListener {
            signOutFromGoogle()
            startActivityForResult(mGoogleSignInClient?.signInIntent, RC_SIGN_IN)
        }
    }

    private fun signOutFromGoogle() {
        mGoogleSignInClient?.signOut()
                ?.addOnCompleteListener(this) { Log.i("Signed Out", "yes") }
    }


    private fun getFacebookData(`object`: JSONObject): Bundle? {
        val bundle = Bundle()
        try {
            val id = `object`.getString("id")
            bundle.putString("idFacebook", id)
            if (`object`.has("first_name"))
                bundle.putString("first_name", `object`.getString("first_name"))
            if (`object`.has("last_name"))
                bundle.putString("last_name", `object`.getString("last_name"))
            if (`object`.has("email"))
                bundle.putString("email", `object`.getString("email"))
//            if (`object`.has("gender"))
//                bundle.putString("gender", `object`.getString("gender"))
//            if (`object`.has("birthday"))
//                bundle.putString("birthday", `object`.getString("birthday"))
        } catch (e: JSONException) {
            Log.e("problem", e.message)
        }
        return bundle
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode === RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callBackManager?.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun socialLogin(socialUserId: String, facebookPassword: String, loginType: LoginType) {

        if(UtilityMethods.isNetworkAvailable(this)){

            if (socialUserId.isNotEmpty() && facebookPassword.isNotEmpty()) {
                val loginModel = LoginDto(socialUserId, facebookPassword, loginType)
                UtilityMethods.showProgressDialog(context)
                APIConnector.socialLogin(loginModel, object : Dispatch {
                    override fun <T> apiSuccess(body: T) {
                        var loginResponse: LoginResponse = body as LoginResponse
                        Log.i("TOKEN", loginResponse.id_token!!)
                        UserCredentials.setToken(applicationContext, loginResponse.id_token!!)
                        UserCredentials.setName(applicationContext, socialLoginName!!)
                        UserCredentials.setEmail(applicationContext, socialLoginEmail!!)
                        if(!loginResponse.mobileNumber.isNullOrEmpty()){
                            UserCredentials.setMobile(applicationContext, loginResponse.mobileNumber)
                        }
                        getUser();
                    }

                    override fun apiError(errorDTO: ErrorDto) {
                        if (errorDTO.fieldErrors != null) {
                            val message = errorDTO.fieldErrors[0].message?.get(0)
                            val field = errorDTO.fieldErrors[0].field
                            UtilityMethods.showToast(applicationContext, "$field: $message")
                        } else {
                            UtilityMethods.showToast(applicationContext, errorDTO.description)
                            Log.e("FACEBOOK LoginnERROR**",errorDTO.description);
                        }
                    }

                    override fun error(body: String?) {
                        UtilityMethods.showToast(applicationContext, body!!)
                    }
                })
            } else {
                UtilityMethods.showToast(applicationContext, "Failed to login")
            }

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }

    private fun getUser() {

        if(UtilityMethods.isNetworkAvailable(this)){

            UtilityMethods.showProgressDialog(this)
            APIConnector.getUser(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    sendFcmTokenToServer()

                    val getUser = body as GetUser
                    when {
                        getUser.reason.equals("EXISTING_USER") -> callMPinActivity()
                        getUser.reason.equals("ELIGIBILITY_FORM") -> callEligibility()
                        getUser.reason.equals("ALREADY_EXISTS") -> confirmDialog()
                        getUser.reason.equals("OTP_VERIFICATION_NEEDED") -> callOTPVerificationActivity()
                        else -> callOTPActivity()
                    }
                }

                override fun apiError(errorDTO: ErrorDto) {
                    if (errorDTO.fieldErrors != null) {
                        val message = errorDTO.fieldErrors[0].message?.get(0)
                        val field = errorDTO.fieldErrors[0].field
                        UtilityMethods.showToast(applicationContext, "$field: $message")
                    } else {
                        UtilityMethods.showToast(applicationContext, errorDTO.description)
                    }
                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }

    private fun callOTPVerificationActivity() {
        startActivity(Intent(this, VerifyOTPActivity::class.java)
                .putExtra("name", socialLoginName)
                .putExtra("email", socialLoginEmail))
        overridePendingTransition(R.anim.enter, R.anim.exit)
    }

    private fun callOTPActivity() {
        startActivity(Intent(this, GenerateOTPActivity::class.java)
                .putExtra("name", socialLoginName)
                .putExtra("email", socialLoginEmail))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        //finish()
    }

    private fun callEligibility() {
        startActivity(Intent(this, EligibilityCriteriaActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callMPinActivity() {
        startActivity(Intent(this, MPinAgainActivity::class.java)
                .putExtra("name", socialLoginName)
                .putExtra("textChange", true))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            val displayName = account?.displayName
            socialLoginEmail = account?.email
            socialLoginName = displayName
            val idToken = account?.idToken + ";" + account?.serverAuthCode
            val id = account?.id


            Log.i("displayName", displayName);
            Log.i("idToken", "" + idToken);
            Log.i("serverAuthCode", "" + account?.serverAuthCode);
            Log.i("id", "" + id);
            socialLogin(id!!, idToken!!, LoginType.GOOGLE)
            // Signed in successfully, show authenticated UI.
            // updateUI(account)
        } catch (e: ApiException) {
            Log.e("G+ Failedss", "" + e.statusCode)
        }
    }

    private fun confirmDialog() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        //dialog.setMessage("Sorry, your account is on hold")
        dialog.setMessage("Sorry, we are unable to process your request. You can apply again after 3 months. To learn more, read FAQs(https://loanit.in) or write to us support@loanit.in")
        //dialog.setMessage("Sorry, we are unable to process your request. To learn more read faqs(https://loanit.in) or write to us support@loanit.in")
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
            UserCredentials.setToken(this, "")
            finish()
        })
        //  dialog.setNegativeButton("CLOSE") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        this.doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show()
    }


    private fun sendFcmTokenToServer(){
        if(UtilityMethods.isNetworkAvailable(this)){

            Log.e("Api Called*********","Called");
            Log.e("Token**********",UserCredentials.getToken(this));
            Log.e("FCMToken**********",UserCredentials.getFcmToken(this));
            val fcmTokenModel=FcmToken(UserCredentials.getFcmToken(this),UserCredentials.getFcmToken(this),"Android","","ANDROID")
            UtilityMethods.showProgressDialog(context)
            APIConnector.sendFcmToken(UserCredentials.getToken(this),fcmTokenModel, object : Dispatch {
                override fun <T> apiSuccess(body: T) {
                    Log.e("Fcm Responce **********",""+body.toString());
                }

                override fun apiError(errorDTO: ErrorDto) {
                    Log.e("Error*********",""+errorDTO);
                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                    Log.e("Error*********2222",""+body);
                }
            });


        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }
}