package com.loanit.app

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.CreateEligibilityForm
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.GenerateOTP
import com.loanit.app.net.requestdto.VerifyOTP
import com.loanit.app.net.responsedto.GenerateOtpResponse
import com.loanit.app.net.responsedto.GetUser
import com.loanit.app.util.AppConstants
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_verify_otp.*

class VerifyOTPActivity : AppCompatActivity(), Dispatch {

    private lateinit var phone: String
    private var otpHalf: String? = null
    private lateinit var name: String
    private lateinit var email: String
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)

        /*otpHalf = intent.getStringExtra("otpHalf")
        val phoneNumber = intent.getStringExtra("phoneNumber")
        name = intent.getStringExtra("name")
        email = intent.getStringExtra("email")
        phone = phoneNumber
        Log.i("otpHalf", otpHalf)*/

        name = UserCredentials.getName(this)
        email= UserCredentials.getEmail(this)
        phone = UserCredentials.getMobile(this)
        val phoneNumber = phone

        tvMobileNumber.text = phone

        btnSubmit.setOnClickListener {
            val otpVal = etOtp.text.toString()
            if (otpVal.isNotEmpty() && !otpHalf.isNullOrEmpty()) {
                if(UtilityMethods.isNetworkAvailable(this)){

                    var finalOtp = otpHalf + otpVal
                    val verifyOTP = VerifyOTP(phoneNumber, finalOtp)
                    UtilityMethods.showProgressDialog(this)
                    APIConnector.verifyOTPNew(UserCredentials.getToken(this),verifyOTP, this)

                }else{
                    UtilityMethods.showToast(this, "Please check your internet connection.")
                }

            }else if(otpVal.isNullOrEmpty()){
                UtilityMethods.showToast(applicationContext, "Please enter OTP.")
            }else{
                UtilityMethods.showToast(applicationContext, "Please generate OTP.")
            }
        }
        otp.setOnClickListener {
            if(UtilityMethods.isNetworkAvailable(this)){

                val generateOTP = GenerateOTP(phoneNumber)
                UtilityMethods.showProgressDialog(this)
                APIConnector.generateOTP(UserCredentials.getToken(this), generateOTP, object : Dispatch {
                    override fun <T> apiSuccess(body: T) {
                        val generateOTPResponseModel = body as GenerateOtpResponse;
                        Log.i("otpHalf", generateOTPResponseModel.otpHalf)
                        otpHalf = generateOTPResponseModel.otpHalf
                    }

                    override fun apiError(error: ErrorDto) {
                    }

                    override fun error(body: String?) {
                    }
                })

            }else{
                UtilityMethods.showToast(this, "Please check your internet connection.")
            }

        }

        tvPrivacyPolicy.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.PRIVACY))
            startActivity(browserIntent)
        }

        tvTerms.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.TERMS))
            startActivity(browserIntent)
        }
    }

    override fun <T> apiSuccess(body: T) {

        confirmDialog()


    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
    }

    private fun confirmDialog() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        //dialog.setMessage("Your mobile number is successfully verified. As mentioned in the description, we are available only in Hyderabad, Bangalore, Pune and Chennai. Other users, please drop us an email to support@loanit.in. We will notify you once we start operations in your city. Thank you.")
        dialog.setMessage("Your mobile number is successfully verified.")

        dialog.setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
            /*UserCredentials.setMobile(applicationContext, phone!!)
            startActivity(Intent(this, EligibilityCriteriaActivity::class.java)
                    .putExtra("phone", phone)
                    .putExtra("email", email)
                    .putExtra("name", name))
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()*/
            startActivity(Intent(this, MPinActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finish()
        })
        //dialog.setNegativeButton("CLOSE") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        this.doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show()
    }
}
