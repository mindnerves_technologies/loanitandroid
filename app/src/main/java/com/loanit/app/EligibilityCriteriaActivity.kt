package com.loanit.app

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.enum.*
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.SaveEligibilityForm
import com.loanit.app.net.responsedto.DummyResponse
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_eligibility_criteria.*
import java.text.SimpleDateFormat
import java.util.*


class EligibilityCriteriaActivity : AppCompatActivity(), Dispatch, DatePickerDialog.OnDateSetListener {

    private val myCalendar = Calendar.getInstance()
    private var doubleBackToExitPressedOnce = false

    var employment = EmploymentType.NONE
    var loanReasonType = LoanReasonType.NONE
    var departmentType = DepartmentType.NONE
    var industryType = IndustryType.NONE
    var workExperienceType = WorkExperienceType.NONE
    var cityType = CityType.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eligibility_criteria)

        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        etName.text = UserCredentials.getName(this)
        etEmail.text = UserCredentials.getEmail(this)
        etContact.setText(UserCredentials.getMobile(this))

        // etName.isEnabled = false
        //  etEmail.isEnabled = false
        etContact.isEnabled = false

        etPan.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val panVal = etPan.text.toString()
                if (panVal.length != 10) {
                    etPan.error = "Enter valid PAN number"
                }
            }
        }
        etSalary.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val salaryVal = etSalary.text.toString()
                if (salaryVal.length < 4) {
                    etSalary.error = "Minimum salary must be 10000 or above"
                }
            }
        }
        etDob.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val dobVal = etDob.text.toString()
                if (dobVal.isEmpty()) {
                    etDob.error = "Date of birth is mandatory"
                }
            }
        }
        etCompanyName.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val companyNameVal = etCompanyName.text.toString()
                if (companyNameVal.isEmpty()) {
                    etCompanyName.error = "Company name is mandatory"
                }
            }
        }
        etDesignation.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val designationVal = etDesignation.text.toString()
                if (designationVal.isEmpty()) {
                    etDesignation.error = "Designation is mandatory"
                }
            }
        }

        etContact.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val contactVal = etContact.text.toString()
                if (contactVal.length != 10) {
                    etContact.error = "Enter valid mobile number"
                }
            }
        }
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        val logout = findViewById<View>(R.id.logout) as TextView
        toolbarText.text = "LoanIt"
        logout.setOnClickListener {

            confirmDialog()

        }
        btnSaveForm.setOnClickListener {

            if(UtilityMethods.isNetworkAvailable(this)){

                val nameVal = etName.text.toString()
                val emailVal = etEmail.text.toString()
                val companyNameVal = etCompanyName.text.toString()
                val designationVal = etDesignation.text.toString()
                val panVal = etPan.text.toString()
                val dobVal = etDob.text.toString()
                val addressVal = etAddress.text.toString()
                val contactVal = etContact.text.toString()
                val salaryVal = etSalary.text.toString()
                val officeAddressVal = etofficeAddress.text.toString()

                if (nameVal.isNotEmpty() && emailVal.isNotEmpty() &&
                        companyNameVal.isNotEmpty() && designationVal.isNotEmpty() &&
                        panVal.isNotEmpty() && dobVal.isNotEmpty() && contactVal.isNotEmpty()) {
                    if (panVal.length == 10) {
                        if (salaryVal.length > 4) {

                            when {
                                cityType == CityType.NONE -> {
                                    UtilityMethods.showToast(this, "Select city")
                                    return@setOnClickListener
                                }
                                loanReasonType == LoanReasonType.NONE -> {
                                    UtilityMethods.showToast(this, "Select what do you need a cash advance for?")
                                    return@setOnClickListener
                                }
                                employment == EmploymentType.NONE -> {
                                    UtilityMethods.showToast(this, "Select employment type")
                                    return@setOnClickListener
                                }
                                industryType == IndustryType.NONE -> {
                                    UtilityMethods.showToast(this, "Select industry type")
                                    return@setOnClickListener
                                }
                                departmentType == DepartmentType.NONE -> {
                                    UtilityMethods.showToast(this, "Select department type")
                                    return@setOnClickListener
                                }
                                workExperienceType == WorkExperienceType.NONE -> {
                                    UtilityMethods.showToast(this, "Select work experience")
                                    return@setOnClickListener
                                }
                                else -> {
                                    UtilityMethods.showProgressDialog(this)
                                    val saveEligibilityForm = SaveEligibilityForm(addressVal, cityType, workExperienceType, employment,
                                            departmentType, loanReasonType, industryType, companyNameVal,
                                            contactVal, dobVal, designationVal, emailVal,
                                            nameVal, officeAddressVal, panVal, salaryVal.toFloat())
                                    APIConnector.saveEligibilityForm(UserCredentials.getToken(this), saveEligibilityForm, this)
                                }
                            }

                        } else {
                            UtilityMethods.showToast(this, "Minimum salary must be 10000 or above")
                        }
                    } else {
                        UtilityMethods.showToast(this, "Enter valid PAN Number")
                    }
                } else {
                    UtilityMethods.showToast(this, "Fill the mandatory fields")
                }

            }else{
                UtilityMethods.showToast(this, "Please check your internet connection.")
            }

        }

        etDob.setOnClickListener {
            setDOBUser()
        }

        employmentSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                when (position) {
                    1 -> employment = EmploymentType.Salaried
                    2 -> employment = EmploymentType.Self_Employed
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                employment = EmploymentType.NONE

            }
        }

        departmentSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    1 -> departmentType = DepartmentType.Technology
                    2 -> departmentType = DepartmentType.Sales
                    3 -> departmentType = DepartmentType.Customer_Support
                    4 -> departmentType = DepartmentType.Operations
                    5 -> departmentType = DepartmentType.Legal
                    6 -> departmentType = DepartmentType.Others
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                departmentType = DepartmentType.NONE
            }
        }

        workExperienceSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    1 -> workExperienceType = WorkExperienceType.Less_Six_Month
                    2 -> workExperienceType = WorkExperienceType.Six_Twelve_Month
                    3 -> workExperienceType = WorkExperienceType.Twelve_Eighteen_Month
                    4 -> workExperienceType = WorkExperienceType.Eighteen_TwentyFour_Month
                    5 -> workExperienceType = WorkExperienceType.TwentyFour_ThirtySix_Month
                    6 -> workExperienceType = WorkExperienceType.More_ThirtySix_Month
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                workExperienceType = WorkExperienceType.NONE

            }
        }

        industrySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    1 -> industryType = IndustryType.Information_Technology
                    2 -> industryType = IndustryType.Manufacturing
                    3 -> industryType = IndustryType.Government_Services
                    4 -> industryType = IndustryType.Teaching
                    5 -> industryType = IndustryType.Financial_Services
                    6 -> industryType = IndustryType.Logistics
                    7 -> industryType = IndustryType.Others
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                industryType = IndustryType.NONE
            }
        }

        loanSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    0 -> loanReasonType = LoanReasonType.NONE
                    1 -> loanReasonType = LoanReasonType.Medical_Emergency
                    2 -> loanReasonType = LoanReasonType.Shopping
                    3 -> loanReasonType = LoanReasonType.Travel_Expenses
                    4 -> loanReasonType = LoanReasonType.House_Repair
                    5 -> loanReasonType = LoanReasonType.Credit_Card_Bill_Payment
                    6 -> loanReasonType = LoanReasonType.Other_Reasons
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                loanReasonType = LoanReasonType.NONE
            }
        }


        citySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    1 -> cityType = CityType.Bangalore
                    2 -> cityType = CityType.Hyderabad
                    3 -> cityType = CityType.Chennai
                    4 -> cityType = CityType.Pune

                    5 -> cityType = CityType.Delhi
                    6 -> cityType = CityType.Gurgaon
                    7 -> cityType = CityType.Mumbai
                    8 -> cityType = CityType.Noida

                    9 -> cityType = CityType.Kolkata
                    10 -> cityType = CityType.Bhubaneswar
                    11 -> cityType = CityType.Coimbatore
                    12 -> cityType = CityType.Cochin
                    13 -> cityType = CityType.Vizag
                    14 -> cityType = CityType.Ahmedabad
                    15 -> cityType = CityType.Indore
                    16 -> cityType = CityType.Delhi
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                cityType = CityType.NONE

            }
        }
    }

    override fun <T> apiSuccess(body: T) {


        val dummyResponse = body as DummyResponse

        if (dummyResponse.reason.equals("Success")) {
            confirmDialog("Congratulations, your membership is approved. Please continue to apply for a loan.", false)
        } else {
            confirmDialog(dummyResponse.reason!!, true)
        }

    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
            closeConfirmDialog(message!!)
        } else {
            closeConfirmDialog(errorDTO.description!!)
//            UtilityMethods.showToast(applicationContext, errorDTO.description)
//            finishAffinity()
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)
    }

    private fun callDashBoard() {
        startActivity(Intent(this, MPinActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callOTPVerification() {
        startActivity(Intent(this, VerifyOTPActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }


    private fun setDOBUser() {
        val mDialog = DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, this, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
        val datePicker = mDialog.datePicker
        datePicker.maxDate = System.currentTimeMillis()
        mDialog.show()
    }


    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, month)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        this.updateLabel()
    }

    private fun updateLabel() {
        val myFormatServer = "yyyy-MM-dd"

        val sdfServer = SimpleDateFormat(myFormatServer, Locale.getDefault())

        etDob.text = sdfServer.format(myCalendar.time)

        //serverDOB = sdfServer.format(myCalendar.time)
    }


    private fun confirmDialog(message: String, close: Boolean) {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setMessage(message)
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
            if (close) {
                UserCredentials.setToken(this, "")
                finish()
            } else {
                //callDashBoard()
                callOTPVerification()
            }
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    private fun closeConfirmDialog(message: String) {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setMessage(message)
        dialog.setPositiveButton("Close App", DialogInterface.OnClickListener { _, _ ->
            finishAffinity()
        })
        dialog.setCancelable(false)
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        this.doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show()
    }

    private fun confirmDialog() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        dialog.setMessage(resources.getString(R.string.logout))
        dialog.setPositiveButton(resources.getString(R.string.yes_1), DialogInterface.OnClickListener { _, _ ->
            UserCredentials.setToken(this, "")
            UtilityMethods.showToast(this, "User Logged Out...")
            startActivity(Intent(this, LoginActivity::class.java))
            overridePendingTransition(R.anim.enter, R.anim.exit)
            finishAffinity()
        })

        dialog.setNegativeButton("NO", DialogInterface.OnClickListener { _, _ ->
        })
        dialog.setCancelable(true)
        dialog.show()
    }

}
