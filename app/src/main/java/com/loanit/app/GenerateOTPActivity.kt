package com.loanit.app

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.CreateEligibilityForm
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.GenerateOTP
import com.loanit.app.net.responsedto.GenerateOtpResponse
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_generate_otp.*



class GenerateOTPActivity : AppCompatActivity(), Dispatch {

    private var doubleBackToExitPressedOnce = false

    private var mobileNumber: String? = null
    private var name: String? = null
    private var email: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generate_otp)
        name = intent.getStringExtra("name")
        email = intent.getStringExtra("email")
        btnGenerateOtp.setOnClickListener {
            val mobileVal = etMobile.text.toString();
            mobileNumber = mobileVal
            if (mobileVal.length == 10) {

                if(UtilityMethods.isNetworkAvailable(this)){

                    UserCredentials.setMobile(applicationContext, mobileVal!!)
                    val generateOTP = GenerateOTP(mobileVal)
                    UtilityMethods.showProgressDialog(this)
                    APIConnector.setMobileNumber(UserCredentials.getToken(this), generateOTP, this)

                }else{
                    UtilityMethods.showToast(this, "Please check your internet connection.")
                }



            } else {
                UtilityMethods.showToast(this, "Enter 10 digit mobile number")
            }
        }
    }


    override fun <T> apiSuccess(body: T) {

        val generateOTPResponseModel = body as GenerateOtpResponse;
        //Log.i("otpHalf", generateOTPResponseModel.otpHalf)

        generateEligibilityFrom();

    }

    private fun generateEligibilityFrom() {
        if(UtilityMethods.isNetworkAvailable(this)){

            val createEligibilityForm = CreateEligibilityForm(name, email, mobileNumber)
            UtilityMethods.showProgressDialog(this)
            APIConnector.createEligibilityForm(UserCredentials.getToken(this), createEligibilityForm, object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    gotoEligibility();
                }

                override fun apiError(errorDTO: ErrorDto) {
                    if (errorDTO.fieldErrors != null) {
                        val message = errorDTO.fieldErrors[0].message?.get(0)
                        val field = errorDTO.fieldErrors[0].field
                        UtilityMethods.showToast(applicationContext, "$field: $message")
                    } else {
                        UtilityMethods.showToast(applicationContext, errorDTO.description)
                    }
                }

                override fun error(body: String?) {
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }

    private fun gotoEligibility() {
        startActivity(Intent(this, EligibilityCriteriaActivity::class.java)
                //.putExtra("otpHalf", generateOTPResponseModel.otpHalf)
                .putExtra("name", name)
                .putExtra("email", email)
                .putExtra("phone", mobileNumber))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
    }

    override fun onResume() {
        super.onResume()
        this.doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Press twice to go to previous screen", Toast.LENGTH_SHORT).show()
    }
}
