package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class MembershipType {

    Basic,Standard,Elite,Platinum,PlatinumPlus
}