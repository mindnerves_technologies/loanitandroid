package com.loanit.app.net.requestdto

/**
 * Created by hocrox_java on 13/10/18.
 */
class CreateEligibilityForm(

        var name: String?,
        var email: String?,
        var mobile: String?
)
