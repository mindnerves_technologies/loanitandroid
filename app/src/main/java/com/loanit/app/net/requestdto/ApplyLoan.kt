package com.loanit.app.net.requestdto

/**
 * Created by hocrox_java on 19/11/18.
 */
class ApplyLoan(

        var amount: Int,
        var tenure: Int,
        var processingFee:Float,
        var totalEmi:Int,
        var iAgreeTime: String
)
