package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class DepartmentType {

    Technology, Sales, Customer_Support, Operations, Legal, Others, NONE
}