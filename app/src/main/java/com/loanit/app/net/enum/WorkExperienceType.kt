package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class WorkExperienceType {

    Less_Six_Month, Six_Twelve_Month, Twelve_Eighteen_Month, Eighteen_TwentyFour_Month,
    TwentyFour_ThirtySix_Month, More_ThirtySix_Month, NONE
}