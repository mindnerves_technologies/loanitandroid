package com.loanit.app.net.responsedto

import com.loanit.app.net.enum.DepartmentType
import com.loanit.app.net.enum.IndustryType
import com.loanit.app.net.enum.MembershipType

/**
 * Created by hocrox_java on 17/10/18.
 */
class UserEligibilityDetails {

    var name: String? = null
    var email: String? = null
    var dateOfBirth: String? = null
    var contact: String? = null
    var address: String? = null
    var panNumber: String? = null
    var companyName: String? = null
    var officeAddress: String? = null
    var eligibilityAmount: Int? = null
    var membershipType: MembershipType? = null
    var departmentType:DepartmentType ? = null
    var industryType:IndustryType ? = null
    var salary: Float? = null
    var totalCredit: Int? = null
}