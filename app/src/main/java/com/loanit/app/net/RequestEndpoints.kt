package com.loanit.app.net

import com.loanit.app.net.requestdto.*
import com.loanit.app.net.responsedto.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface RequestEndpoints {

    @POST("login")
    fun socialLogin(@Body loginModel: LoginDto): Call<LoginResponse>

    @POST("eligibilityForm/create")
    fun createEligibilityForm(@Header("Authorization") token: String, @Body eligibilityForm: CreateEligibilityForm): Call<DummyResponse>

    @POST("account/generateOTP")
    fun generateOTP(@Header("Authorization") token: String, @Body generateOTP: GenerateOTP): Call<GenerateOtpResponse>

    @POST("account/setContact")
    fun setContact(@Header("Authorization") token: String, @Body generateOTP: GenerateOTP): Call<GenerateOtpResponse>


    @POST("eligibilityForm/save")
    fun saveEligibilityForm(@Header("Authorization") token: String, @Body saveEligibilityForm: SaveEligibilityForm): Call<DummyResponse>

    @POST("account/verifyOTP")
    fun verifyOTP(@Body verifyOTP: VerifyOTP): Call<VerifyOtpResponse>

    @POST("account/newVerifyOTP")
    fun verifyOTPNew(@Header("Authorization") token: String,@Body verifyOTP: VerifyOTP): Call<VerifyOtpResponse>


    @POST("completeProfile/save")
    fun completeProfile(@Header("Authorization") token: String, @Body saveProfile: SaveProfile): Call<DummyResponse>

    @POST("account/set/mpin")
    fun setMPin(@Header("Authorization") token: String, @Query("mpin") mpin: String): Call<DummyResponse>

    @POST("account/reset_mpin/finish")
    fun resetMPinFinish(@Header("Authorization") token: String, @Body resetMpinFinish: ResetMpinFinish): Call<DummyResponse>

    @POST("account/reset_mpin/init")
    fun resetMPinInit(@Header("Authorization") token: String, @Body resetMpinInit: ResetMpinInit): Call<DummyResponse>

    @GET("account/get/user")
    fun getUser(@Header("Authorization") token: String): Call<GetUser>

    @GET("eligibilityForm/getOne")
    fun getEligibilityForm(@Header("Authorization") token: String): Call<UserEligibilityDetails>

    @GET("loan/all/history")
    fun loanHistory(@Header("Authorization") token: String): Call<List<LoanHistoryResponse>>

    @GET("loanEmi/get/loanHistory")
    fun loanHistoryEmi(@Header("Authorization") token: String): Call<List<LoanHistoryResponse>>

    @GET("loan/all/active")
    fun activeLoans(@Header("Authorization") token: String): Call<List<LoanStatusResponse>>

    @GET("loanEmi/get/loan/status")
    fun activeLoansEmi(@Header("Authorization") token: String): Call<List<LoanStatusResponse>>

    @GET("eligibilityForm/getOne")
    fun getProfile(@Header("Authorization") token: String): Call<GetProfile>

    @GET("completeProfile/getProfile")
    fun getProfileAnother(@Header("Authorization") token: String): Call<GetProfile>

    @POST("loan/apply")
    fun applyLoan(@Header("Authorization") token: String, @Body applyLoan: ApplyLoan): Call<DummyResponse>

    @GET("loanEmi/loanEmi/get/newLoanEmis/{amount}")
    fun getLoanEMI(@Header("Authorization") token: String,@Path("amount") amount:Int): Call<LoanEmiResponse>

    @GET("notification/getlist")
    fun getNotificationList(@Header("Authorization") token: String): Call<List<NotificationList>>

    @GET("notification/getunreadcount")
    fun getUnReadCount(@Header("Authorization") token: String): Call<Int>


    @POST("notification/changereadstatus")
    fun changeReadStatus(@Header("Authorization") token: String): Call<DummyResponse>

    @POST("device/add")
    fun sendFcmToken(@Header("Authorization") token: String, @Body fcmToken: FcmToken): Call<DummyResponse>

    @POST("notification/deleteNotification")
    fun deleteNotifications(@Header("Authorization") token: String): Call<DummyResponse>


    @POST("usercontacts/addall")
    fun sendContact(@Header("Authorization") token: String, @Body contacts: ArrayList<UserContacts>): Call<DummyResponse>


}