package com.loanit.app.net.responsedto

import com.loanit.app.net.enum.LoanStatus

/**
 * Created by hocrox_java on 20/11/18.
 */
class LoanHistoryResponse {

    var amount: Float = 0f
    var exhaustedDays: String? = null
    var id: Int = 0
    var loanId: Int = 0
    var interest: Float = 0f
    var totalAmount: Float = 0f
    var tenure: Int = 0
    var loanStatus: LoanStatus? = null
    var loanDate: String? = null
    var repaidDate: String? = null
    var amountPaid: Float = 0.0f
    var loanClearedDate: String? = null
    var membershipType: String? = null
    var loanPreClosed: Boolean = false


    //Added new field "EMI record" for updated subscription "Platinum and Platinum +"
    val loanEmiUser = ArrayList<EmiResponse>()
    val totalCreditPoint: Int = 0

}