package com.loanit.app.net.requestdto

import com.loanit.app.net.enum.LoginType

/**
 * Created by hocrox_java on 13/10/18.
 */
open class LoginDto(

        var username: String,
        var password: String,
        var loginType: LoginType
)
