package com.loanit.app.net.responsedto

import com.loanit.app.net.enum.LoanStatus

/**
 * Created by hocrox_java on 20/11/18.
 */
class LoanStatusResponse {

    var amount: Float = 0f
    var exhaustedDays: Int = 0
    var id: Int = 0
    var loanId: Int = 0
    var interest: Float = 0f
    var totalAmount: Float = 0f
    var tenure: Int = 0
    var loanStatus: LoanStatus? = null
    var loanDate: String? = null

    //Added new field "EMI record" for updated subscription "Platinum and Platinum +"
    val loanEmiUser = ArrayList<EmiResponse>()
    var approvalDate: String? = null
    var membershipType: String? = null
}