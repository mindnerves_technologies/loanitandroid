package com.loanit.app.net.responsedto

class LoginResponse {
    var id_token: String? = null
    var userType: String? = null
    var mobileNumber: String = ""
}
