package com.loanit.app.net

import com.loanit.app.net.requestdto.ErrorDto


interface Dispatch {
    fun <T>apiSuccess(body: T)
    fun apiError(error: ErrorDto)
    fun error(body: String?)
}
