package com.loanit.app.net.requestdto

/**
 * Created by hocrox_java on 01/12/18.
 */
open class ResetMpinFinish (

        var key :String,
        var newPassword :String
)
