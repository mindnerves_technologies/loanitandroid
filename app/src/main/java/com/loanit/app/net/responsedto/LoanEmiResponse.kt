package com.loanit.app.net.responsedto

import com.loanit.app.net.enum.LoanStatus

class LoanEmiResponse {

    val platinum_emis = ArrayList<EmiResponse>()

    val platinum_plus_emis = ArrayList<EmiResponse>()

}