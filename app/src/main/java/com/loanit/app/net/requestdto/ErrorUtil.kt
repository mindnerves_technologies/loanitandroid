package com.loanit.app.net.requestdto

import com.loanit.app.util.AppConstants


/**
 * Created by hocrox_java on 08/05/18.
 */
class ErrorUtil {

    companion object {
        private var errorDTO: ErrorDto? = null
        fun getErrorDetails(failureResponseJson: String): ErrorDto {
            try {
                errorDTO = AppConstants.gson.fromJson(failureResponseJson, ErrorDto::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
               errorDTO?.exceptionMessage = e.message!!
                return errorDTO!!
            }
            return errorDTO!!
        }
    }
}