package com.loanit.app.net.requestdto

import lombok.Getter
import lombok.Setter

/**
 * Created by hocrox_java on 08/05/18.
 */


@Getter
@Setter
class FieldErrorDto {

     lateinit var objectName: String
     lateinit var field: String
     val message: List<String>? = null

}