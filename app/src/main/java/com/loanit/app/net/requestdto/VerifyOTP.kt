package com.loanit.app.net.requestdto

/**
 * Created by hocrox_java on 13/10/18.
 */
open class VerifyOTP(

        var phoneNumber: String,
        var otp: String
)
