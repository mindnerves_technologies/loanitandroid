package com.loanit.app.net.requestdto

open class FcmToken(
    var deviceID: String,
    var messagingID: String,
    var deviceName: String,
    var ownerId: String ,
    var deviceType: String

)