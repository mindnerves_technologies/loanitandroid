package com.loanit.app.net.requestdto

import lombok.Getter
import lombok.Setter

/**
 * Created by hocrox_java on 08/05/18.
 */
//https://www.callicoder.com/kotlin-nullable-types-null-safety/


@Getter
@Setter
class ErrorDto {

    var message : String = ""
    var description : String = ""
    var exceptionMessage : String = ""
    val fieldErrors: List<FieldErrorDto>? = null

}