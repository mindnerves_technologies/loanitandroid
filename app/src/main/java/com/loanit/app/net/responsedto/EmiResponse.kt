package com.loanit.app.net.responsedto

import com.loanit.app.net.enum.LoanStatus

class EmiResponse {

    var version: Int = 0
    var id: Int = 0
    var emiAmount: Float = 0f
    var emiProcessingFee: Float = 0f
    var emiTenure: Int = 0
    var emiInterest: Float = 0f
    var emiPenalty: Float = 0f
    var totalEmiAmount: Float = 0f
    var emiStatus: LoanStatus? = null
    var emiStartDate: String? = null
    var emiDueDate: String? = null
    var comment: String? = null
    var emiRepaidDate: String? = null
    var loanId: Int = 0
    var emiCreditPoint: Int = 0
}