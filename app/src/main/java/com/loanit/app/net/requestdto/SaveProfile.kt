package com.loanit.app.net.requestdto

import com.loanit.app.net.enum.HolidayType
import com.loanit.app.net.enum.RentType
import com.loanit.app.net.enum.TravelType

/**
 * Created by hocrox_java on 17/11/18.
 */
data class SaveProfile(
        var aadharBackImage: String,
        var aadharFrontImage: String,
        var aadharNumber: String,
        var accountNumber: String,
        var alternateEmail: String,
        var alternateMobile: String,
        var bankStatement: String,
        var degree: String,
        var holidayType: HolidayType,
        var ifscCode: String,
        var officeEmail: String,
        var officeNumber: String,
        var passoutYear: String,
        var rentType: RentType,
        var travelType: TravelType,
        var salarySlipImage: String,
        val contactDTOList: List<ContactList>? = null,
        var bankStatementPassword: String,
        var salarySlipPassword: String

)