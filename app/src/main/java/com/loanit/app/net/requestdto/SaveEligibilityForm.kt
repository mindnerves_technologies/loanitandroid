package com.loanit.app.net.requestdto

import com.loanit.app.net.enum.*

/**
 * Created by hocrox_java on 13/10/18.
 */
data class SaveEligibilityForm(

        var address: String,
        var cityType: CityType,
        var workExperienceType: WorkExperienceType,
        var employmentType: EmploymentType,
        var departmentType: DepartmentType,
        var loanReasonType: LoanReasonType,
        var industryType: IndustryType,
        var companyName: String,
        var contact: String,
        var dateOfBirth: String,
        var designation: String,
        var email: String,
        var name: String,
        var officeAddress: String,
        var panNumber: String,
        var salary: Float

)
