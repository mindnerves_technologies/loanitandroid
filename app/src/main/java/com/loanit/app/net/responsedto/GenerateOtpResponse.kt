package com.loanit.app.net.responsedto

/**
 * Created by hocrox_java on 17/10/18.
 */
class GenerateOtpResponse {

    var message: String? = null

    var newUser: Boolean = false

    var otpHalf: String? = null
}