package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 20/11/18.
 */
enum class LoanStatus {

    APPROVED, CLEARED, IN_PROCESS, CANCELLED
}