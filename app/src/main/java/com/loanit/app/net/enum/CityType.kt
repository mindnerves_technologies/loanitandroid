package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class CityType {

    Bangalore, Chennai, Delhi, Gurgaon, Hyderabad, Mumbai, Noida, Pune, Kolkata, Bhubaneswar, Coimbatore, Cochin, Vizag, Ahmedabad,
    Indore, NONE
}

