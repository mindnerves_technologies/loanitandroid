package com.loanit.app.net;

import com.loanit.app.net.requestdto.*
import com.loanit.app.net.responsedto.*
import javax.xml.transform.OutputKeys


/**
 * Created by hocrox_java on 22/04/17.
 */

class APIConnector {

    companion object {

        fun socialLogin(loginDto: LoginDto, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().socialLogin(loginDto).enqueue(RequestCallback<LoginResponse>(dispatch))
        }

        fun createEligibilityForm(token: String, eligibilityForm: CreateEligibilityForm, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().createEligibilityForm(token, eligibilityForm).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun saveEligibilityForm(token: String, saveEligibilityForm: SaveEligibilityForm, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().saveEligibilityForm(token, saveEligibilityForm).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun completeProfile(token: String, saveProfile: SaveProfile, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().completeProfile(token, saveProfile).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun generateOTP(token: String, generateOTP: GenerateOTP, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().generateOTP(token, generateOTP).enqueue(RequestCallback<GenerateOtpResponse>(dispatch))
        }

        fun setMobileNumber(token: String, generateOTP: GenerateOTP, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().setContact(token, generateOTP).enqueue(RequestCallback<GenerateOtpResponse>(dispatch))
        }

        fun verifyOTP(verifyOTP: VerifyOTP, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().verifyOTP(verifyOTP).enqueue(RequestCallback<VerifyOtpResponse>(dispatch))
        }

        fun verifyOTPNew(token: String, verifyOTP: VerifyOTP, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().verifyOTPNew(token,verifyOTP).enqueue(RequestCallback<VerifyOtpResponse>(dispatch))
        }

        fun getUser(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getUser(token).enqueue(RequestCallback<GetUser>(dispatch))
        }

        fun getEligibilityForm(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getEligibilityForm(token).enqueue(RequestCallback<UserEligibilityDetails>(dispatch))
        }

        fun getProfile(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getProfile(token).enqueue(RequestCallback<GetProfile>(dispatch))
        }

        fun getProfileAnother(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getProfileAnother(token).enqueue(RequestCallback<GetProfile>(dispatch))
        }

        fun loanHistory(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().loanHistory(token).enqueue(RequestCallback<List<LoanHistoryResponse>>(dispatch))
        }

        fun loanHistoryEmi(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().loanHistoryEmi(token).enqueue(RequestCallback<List<LoanHistoryResponse>>(dispatch))
        }

        fun activeLoans(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().activeLoans(token).enqueue(RequestCallback<List<LoanStatusResponse>>(dispatch))
        }


        fun activeLoansEmi(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().activeLoansEmi(token).enqueue(RequestCallback<List<LoanStatusResponse>>(dispatch))
        }

        fun setMPin(token: String, mPin: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().setMPin(token, mPin).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun resetMPinInit(token: String, resetMpinInit: ResetMpinInit, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().resetMPinInit(token, resetMpinInit).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun resetMPinFinish(token: String, resetMpinFinish: ResetMpinFinish, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().resetMPinFinish(token, resetMpinFinish).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun applyLoan(token: String, applyLoan: ApplyLoan, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().applyLoan(token, applyLoan).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun getEMI(token: String,amount: Int, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getLoanEMI(token, amount).enqueue(RequestCallback<LoanEmiResponse>(dispatch))
        }

        fun getNotificationList(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getNotificationList(token).enqueue(RequestCallback<List<NotificationList>>(dispatch))
        }

        fun getUnReadCount(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().getUnReadCount(token).enqueue(RequestCallback<Int>(dispatch))
        }

        fun changeReadStatus(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().changeReadStatus(token).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun sendFcmToken(token: String, fcmToken: FcmToken, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().sendFcmToken(token, fcmToken).enqueue(RequestCallback<DummyResponse>(dispatch))
        }

        fun deleteNotifications(token: String, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().deleteNotifications(token).enqueue(RequestCallback<DummyResponse>(dispatch))
        }


        fun sendAllContacts(token: String, contacts:ArrayList<UserContacts>, dispatch: Dispatch) {
            InitApi.getRequestEndPoints().sendContact(token,contacts).enqueue(RequestCallback<DummyResponse>(dispatch))
        }
    }
}
