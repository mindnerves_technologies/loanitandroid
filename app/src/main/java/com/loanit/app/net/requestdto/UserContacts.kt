package com.loanit.app.net.requestdto

data class UserContacts (

    var name:String,
    var mobile:String

)