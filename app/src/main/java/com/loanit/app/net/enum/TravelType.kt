package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/11/18.
 */
enum class TravelType {
    PUBLIC_TRANSPORT, OWN_VEHICLE, WITH_FRIENDS, CAB, NOTHING
}