package com.loanit.app.net

import android.util.Log
import com.loanit.app.net.requestdto.ErrorUtil
import com.loanit.app.util.AppConstants
import com.loanit.app.util.UtilityMethods
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by hocrox_java on 08/05/18.
 */
class RequestCallback<T>(private val dispatch: Dispatch) : Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {
        UtilityMethods.dismissProgressDialog()
        Log.i("RESPONSE", AppConstants.gson.toJson(response.body()))
        if (response.isSuccessful) {
            dispatch.apiSuccess(response.body())
        } else {
            try {
                Log.e("RESPONSE", AppConstants.gson.toJson(response.errorBody()))
                val errorDTO = ErrorUtil.getErrorDetails(response.errorBody().string())
                dispatch.apiError(errorDTO)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    override fun onFailure(call: Call<T>, t: Throwable?) {
        UtilityMethods.dismissProgressDialog()
        if (t != null) {
            dispatch.error(t.message!!)
        }
    }
}
