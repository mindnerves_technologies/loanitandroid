package com.loanit.app.net.responsedto

/**
 * Created by hocrox_java on 21/11/18.
 */
class GetProfile {
    var salary: String? = null
    var contact: String? = null
    var companyName: String? = null
    var email: String? = null
    var address: String? = null
    var accountNumber: String? = null
    var name: String? = null

}
