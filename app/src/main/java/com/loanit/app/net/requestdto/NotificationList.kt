package com.loanit.app.net.requestdto

import com.loanit.app.net.enum.LoanStatus

class NotificationList {

    var title: String? = null
    var message: String? = null
    var id: Long? = null
    var read: Boolean? = false
    var date: String? = null



}