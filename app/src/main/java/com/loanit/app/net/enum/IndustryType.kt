package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class IndustryType {

    Information_Technology, Manufacturing, Government_Services, Teaching, Financial_Services, Logistics, Others, NONE
}