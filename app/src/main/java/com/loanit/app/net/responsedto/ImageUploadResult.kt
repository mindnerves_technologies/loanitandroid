package com.loanit.app.net.responsedto

import lombok.Getter
import lombok.Setter

/**
 * Created by hocrox_java on 20/11/18.
 */

@Getter
@Setter
class ImageUploadResult {

    var documentName: String? = null
    var documentOriginalName: String? = null
}