package com.loanit.app.net.requestdto

/**
 * Created by hocrox_java on 17/11/18.
 */
class ContactList {

    var email: String? = null
    var mobile: String? = null
    var name: String? = null
    var relation: String? = null
}
