package com.loanit.app.net.responsedto

import lombok.Getter
import lombok.Setter

/**
 * Created by hocrox_java on 17/11/18.
 */
@Getter
@Setter
class UploadImageModel {

    var result: ImageUploadResult? = null
    var response: Int = 0
}