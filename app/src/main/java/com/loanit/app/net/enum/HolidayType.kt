package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/11/18.
 */
enum class HolidayType {
    NOT_MARRIED, MOVIE_DINNER, AT_HOME, WITH_FRIENDS, READING_BOOKS_SHOPPING, NOTHING
}