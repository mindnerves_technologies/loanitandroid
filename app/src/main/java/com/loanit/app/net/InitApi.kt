package com.loanit.app.net

import com.loanit.app.util.AppConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by hocrox_java on 08/05/18.
 */
class InitApi {

    companion object {
        private var requestEndpoints: RequestEndpoints? = null
        fun getRequestEndPoints(): RequestEndpoints {
            if (requestEndpoints == null) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                val httpClient = OkHttpClient.Builder()
                val okHttpClient = httpClient.connectTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(20, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS).build()

                httpClient.addInterceptor(logging)
                val retrofit = Retrofit.Builder().baseUrl(AppConstants.BASE_URL).addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build()
                requestEndpoints = retrofit.create(RequestEndpoints::class.java)
            }
            return requestEndpoints!!
        }
    }
}