package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/11/18.
 */
enum class RentType {
    DONT_PAY, LESS_THAN_5000, BTW_5000_10000, BTW_10000_15000, BTW_15000_20000, MORE_THAN_20000, NOTHING
}