package com.loanit.app.net.enum

/**
 * Created by hocrox_java on 17/10/18.
 */
enum class LoanReasonType {

    Medical_Emergency, Shopping, Travel_Expenses, House_Repair, Credit_Card_Bill_Payment, Other_Reasons, NONE
}