package com.loanit.app

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.responsedto.GetUser
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import android.util.Base64


class SplashActivity : AppCompatActivity() {

    lateinit var context: Context
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
    private var doubleBackToExitPressedOnce = false

//    Email: carbd002@mymail.unisa.edu.au Password: Airlaunder002
//    https://www.makeuseof.com/tag/new-features-android-pie/
//    https://marsic.info/2016/03/01/android-service-intent-asynctask-thread/
//    https://proandroiddev.com/a-quest-for-comfy-android-app-architecture-pt-2-the-clean-3a96eb8b8429
//    https://www.truiton.com/2014/11/bound-service-example-android/
    //https://www.journaldev.com/20292/android-mvvm-design-pattern

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        context = this

//        try {
//            val info = packageManager.getPackageInfo("com.loanit.app", PackageManager.GET_SIGNATURES)
//            for (signature in info.signatures) {
//                val md = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
//            }
//        } catch (e: PackageManager.NameNotFoundException) {
//            e.printStackTrace()
//        } catch (e: NoSuchAlgorithmException) {
//            e.printStackTrace()
//        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkPermission()) {
                addPermissions()
            } else {
                whatToDo()
            }
        } else {
            whatToDo()
        }

        generatehasshKey();
    }


    private fun whatToDo() {
        object : CountDownTimer(2500, 1000) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                try {
                    var token = UserCredentials.getToken(context)
                    Log.i("TOKENU", token)
                    if (token.length > 15) {
                        getUser()
                    } else {
                        startActivity(Intent(context, LoginActivity::class.java))
                        overridePendingTransition(R.anim.enter, R.anim.exit)
                        finish()
                    }
                } catch (e: Exception) {
                    Log.i("Splash Thread", e.message);
                }
            }

        }.start()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val perms = HashMap<String, Int>()
                // Initial
                perms[android.Manifest.permission.READ_CONTACTS] = PackageManager.PERMISSION_GRANTED
                perms[android.Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
//                perms[android.Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED
//                perms[android.Manifest.permission.RECEIVE_SMS] = PackageManager.PERMISSION_GRANTED
                // Fill with results
                for (i in permissions.indices)
                    perms[permissions[i]] = grantResults[i]
                if (perms[android.Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[android.Manifest.permission.READ_CONTACTS] == PackageManager.PERMISSION_GRANTED
//                        && perms[android.Manifest.permission.CALL_PHONE] == PackageManager.PERMISSION_GRANTED
//                        && perms[android.Manifest.permission.RECEIVE_SMS] == PackageManager.PERMISSION_GRANTED

                ) {
                    // If all permissions allowed
                    whatToDo()
                } else {
                    //whatToDo()
//                    Toast.makeText(this, "You need to allow all permissions to make this app working", Toast.LENGTH_SHORT)
//                            .show()
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS) || ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showDialogOK("Contact & Storage permissions are required to function this app properly. Please allow these permissions.", DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> addPermissions()
                                        DialogInterface.BUTTON_NEGATIVE -> {
                                            finish()
                                        }
                                    }// proceed with logic by disabling the related features or quit the app.
                                })
                    } else {
//                        Toast.makeText(this, "Go to app settings and enable permissions", Toast.LENGTH_LONG)
//                                .show()
                        finish()
                        //                            //proceed with logic by disabling the related features or quit the app.
                    }//permission is denied (and never ask again is  checked)
                    //shouldShowRequestPermissionRationale will return false




                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun addPermissions() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return
        }
        val permissionsNeeded = ArrayList<String>()
        val permissionsList = ArrayList<String>()
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage")
        if (!addPermission(permissionsList, android.Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Contacts")
//        if (!addPermission(permissionsList, android.Manifest.permission.CALL_PHONE))
//            permissionsNeeded.add("Call")
//        if (!addPermission(permissionsList, android.Manifest.permission.RECEIVE_SMS))
//            permissionsNeeded.add("Receive Sms")

        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) {
                var message = "You need to grant access to "
                for (i in 1 until permissionsNeeded.size)
                    message = message + ", " + permissionsNeeded[i]
                requestPermissions(permissionsList.toTypedArray(),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                return
            }
            requestPermissions(permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)
        val result1 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
      //  val result2 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS)
      //  val result3 = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE)

        result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED

        Log.i("status", "msg$result1")
        return result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED

//                result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED

    }

    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission)
                if (!shouldShowRequestPermissionRationale(permission))
                    return false
            }
        }
        return true
    }


    private fun getUser() {
       // UtilityMethods.showProgressDialog(this)
        if(UtilityMethods.isNetworkAvailable(this)){

            APIConnector.getUser(UserCredentials.getToken(this), object : Dispatch {
                override fun <T> apiSuccess(body: T) {

                    val getUser = body as GetUser
                    when {
                        getUser.reason.equals("EXISTING_USER") -> callMPinActivity()
                        getUser.reason.equals("ELIGIBILITY_FORM") -> callEligibility()
                        getUser.reason.equals("ALREADY_EXISTS") -> confirmDialog()
                        getUser.reason.equals("OTP_VERIFICATION_NEEDED") -> callOTPVerificationNeeded()
                        getUser.reason.equals("NEW_USER") -> callLogin()
                        else -> callLogin()
                    }
                }

                override fun apiError(errorDTO: ErrorDto) {
                    if (errorDTO.fieldErrors != null) {
                        val message = errorDTO.fieldErrors[0].message?.get(0)
                        val field = errorDTO.fieldErrors[0].field
                        UtilityMethods.showToast(applicationContext, "$field: $message")
                        callLogin()
                    } else {
                        callLogin()
                        UtilityMethods.showToast(applicationContext, errorDTO.description)
                    }
                }

                override fun error(body: String?) {
                    UtilityMethods.showToast(applicationContext, body!!)
                }
            })

        }else{
            UtilityMethods.showToast(this, "Please check your internet connection.")
        }

    }

    private fun callEligibility() {
        startActivity(Intent(this, EligibilityCriteriaActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }


    private fun callOTPActivity() {
        startActivity(Intent(this, GenerateOTPActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callOTPVerificationNeeded() {
        startActivity(Intent(this, LoginActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    private fun callMPinActivity() {
        startActivity(Intent(this, MPinAgainActivity::class.java)
                .putExtra("textChange", true))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }


    private fun confirmDialog() {
        val dialog = android.support.v7.app.AlertDialog.Builder(this)
        //dialog.setMessage("Sorry, your account is on hold")
        dialog.setMessage(" Sorry, we are unable to process your request. You can apply again after 3 months. To learn more, read FAQs(https://loanit.in) or write to us support@loanit.in")
        //dialog.setMessage("Sorry, we are unable to process your request. To learn more read faqs(https://loanit.in) or write to us support@loanit.in")
        dialog.setPositiveButton(resources.getString(R.string.yes), DialogInterface.OnClickListener { _, _ ->
            UserCredentials.setToken(this, "")
            finish()
        })
        //  dialog.setNegativeButton("CLOSE") { _, _ -> dialog.setOnDismissListener { dialog -> dialog.dismiss() } }
        dialog.setCancelable(false)
        dialog.show()
    }

    override fun onResume() {
        super.onResume()
        this.doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, R.string.exit_press_back_twice_message, Toast.LENGTH_SHORT).show()
    }


    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show()
    }


    private fun generatehasshKey() {
        try {
            val info = packageManager.getPackageInfo(
                    "com.loanit.app",
                    PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }
    }
//    satpalsharma_90@yahoo.com

}
