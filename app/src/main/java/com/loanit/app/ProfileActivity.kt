package com.loanit.app

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.TextView
import com.loanit.app.net.APIConnector
import com.loanit.app.net.Dispatch
import com.loanit.app.net.enum.HolidayType
import com.loanit.app.net.enum.RentType
import com.loanit.app.net.enum.TravelType
import com.loanit.app.net.requestdto.ContactList
import com.loanit.app.net.requestdto.ErrorDto
import com.loanit.app.net.requestdto.SaveProfile
import com.loanit.app.net.responsedto.UploadImageModel
import com.loanit.app.upload.FileUploadService
import com.loanit.app.upload.ServiceGenerator
import com.loanit.app.util.RealPathUtil
import com.loanit.app.util.UserCredentials
import com.loanit.app.util.UtilityMethods
import kotlinx.android.synthetic.main.activity_view_profile.*
import kotlinx.android.synthetic.main.references.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class ProfileActivity : AppCompatActivity(), Dispatch {


     var rentType = RentType.NOTHING
     var holidayType = HolidayType.NOTHING
     var travelType = TravelType.NOTHING
     var degreeVal = "NONE"
     var passOutVal= "NONE"
    var aadharFront: String = ""
    var aadharBack: String = ""
    var aadharSalarySlip: String = ""
    var aadharbankStatement: String = ""
    var attached: Int = 10
    var count = 0
    var oneLayout = true
    var twoLayout = true
    var threeLayout = true
    var fourLayout = true
    var fiveLayout = true
    private var sixLayout = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        val toolbar = findViewById<View>(R.id.tool) as Toolbar
        val addMore = findViewById<TextView>(R.id.addMore)
        val ref4 = findViewById<TextView>(R.id.ref4)
        val ref15 = findViewById<TextView>(R.id.ref15)
        val ref16 = findViewById<TextView>(R.id.ref16)
        val ref1 = findViewById<TextView>(R.id.ref1)
        val ref3 = findViewById<TextView>(R.id.ref3)
        val ref1neeche = findViewById<TextView>(R.id.ref1neeche)
        val lrefu24 = findViewById<LinearLayout>(R.id.lrefu24)
        val lrefu = findViewById<LinearLayout>(R.id.lrefu)
        val lref1 = findViewById<LinearLayout>(R.id.lref1)
        val lref2 = findViewById<LinearLayout>(R.id.lref2)
        val lref3 = findViewById<LinearLayout>(R.id.lref3)
        val lrefu2 = findViewById<LinearLayout>(R.id.lrefu2)
        val lrefu23 = findViewById<LinearLayout>(R.id.lrefu23)
        val lref22 = findViewById<LinearLayout>(R.id.lref22)
        val lref23 = findViewById<LinearLayout>(R.id.lref23)
        val lref32 = findViewById<LinearLayout>(R.id.lref32)
        val lref232 = findViewById<LinearLayout>(R.id.lref232)
        val lref233 = findViewById<LinearLayout>(R.id.lref233)
        val lref2334 = findViewById<LinearLayout>(R.id.lref2334)

        val lrefu56 = findViewById<LinearLayout>(R.id.lrefu56)
        val lref1556 = findViewById<LinearLayout>(R.id.lref1556)
        val lref256 = findViewById<LinearLayout>(R.id.lref256)
        val lref356 = findViewById<LinearLayout>(R.id.lref356)


        val lrefu5 = findViewById<LinearLayout>(R.id.lrefu5)
        val lref155 = findViewById<LinearLayout>(R.id.lref155)
        val lref25 = findViewById<LinearLayout>(R.id.lref25)


        val lref2324 = findViewById<LinearLayout>(R.id.lref2324)
        val lref35 = findViewById<LinearLayout>(R.id.lref35)
        val lref23444 = findViewById<LinearLayout>(R.id.lref23444)
        val lref234 = findViewById<LinearLayout>(R.id.lref234)


        setSupportActionBar(toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.elevation = 0f
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toolbarText = findViewById<View>(R.id.toolbarText) as TextView
        toolbarText.text = "Complete profile"

        addMore.setOnClickListener {
            count++
            if (count == 1) {
                ref4.visibility = View.VISIBLE
                lrefu24.visibility = View.VISIBLE
                lref2324.visibility = View.VISIBLE
                lref234.visibility = View.VISIBLE
                lref23444.visibility = View.VISIBLE
            }
            if (count == 2) {
                ref15.visibility = View.VISIBLE
                lrefu5.visibility = View.VISIBLE
                lref155.visibility = View.VISIBLE
                lref25.visibility = View.VISIBLE
                lref35.visibility = View.VISIBLE
            }
            if (count == 3) {
                ref16.visibility = View.VISIBLE
                lrefu56.visibility = View.VISIBLE
                lref1556.visibility = View.VISIBLE
                lref256.visibility = View.VISIBLE
                lref356.visibility = View.VISIBLE
                addMore.visibility = View.GONE
            }
        }


        ref4.setOnClickListener {
            if (fourLayout) {
                lrefu24.visibility = View.GONE
                lref2324.visibility = View.GONE
                lref234.visibility = View.GONE
                lref23444.visibility = View.GONE
                ref4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                fourLayout = false
            } else {
                lrefu24.visibility = View.VISIBLE
                lref2324.visibility = View.VISIBLE
                lref234.visibility = View.VISIBLE
                lref23444.visibility = View.VISIBLE
                ref4.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                fourLayout = true
            }
        }

        ref15.setOnClickListener {
            if (fiveLayout) {
                lrefu5.visibility = View.GONE
                lref155.visibility = View.GONE
                lref25.visibility = View.GONE
                lref35.visibility = View.GONE
                ref15.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                fiveLayout = false
            } else {
                lrefu5.visibility = View.VISIBLE
                lref155.visibility = View.VISIBLE
                lref25.visibility = View.VISIBLE
                lref35.visibility = View.VISIBLE
                ref15.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                fiveLayout = true
            }
        }

        ref1.setOnClickListener {
            if (oneLayout) {
                lrefu.visibility = View.GONE
                lref1.visibility = View.GONE
                lref2.visibility = View.GONE
                lref3.visibility = View.GONE
                ref1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                oneLayout = false
            } else {
                lrefu.visibility = View.VISIBLE
                lref1.visibility = View.VISIBLE
                lref3.visibility = View.VISIBLE
                lref2.visibility = View.VISIBLE
                ref1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                oneLayout = true
            }
        }
        ref16.setOnClickListener {
            if (sixLayout) {
                lrefu56.visibility = View.GONE
                lref1556.visibility = View.GONE
                lref256.visibility = View.GONE
                lref356.visibility = View.GONE
                ref16.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                sixLayout = false
            } else {
                lrefu56.visibility = View.VISIBLE
                lref1556.visibility = View.VISIBLE
                lref256.visibility = View.VISIBLE
                lref356.visibility = View.VISIBLE
                ref16.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                sixLayout = true
            }
        }
        ref1neeche.setOnClickListener {

            if (twoLayout) {
                lrefu2.visibility = View.GONE
//                refviewu2.visibility = View.GONE
                lref22.visibility = View.GONE
//                refview22.visibility = View.GONE
                lref23.visibility = View.GONE
//                refview23.visibility = View.GONE
                lref32.visibility = View.GONE
//                ref3.visibility = View.GONE
                ref1neeche.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                twoLayout = false
            } else {
                lrefu2.visibility = View.VISIBLE
//                refviewu2.visibility = View.VISIBLE
                lref22.visibility = View.VISIBLE
//                refview22.visibility = View.VISIBLE
                lref23.visibility = View.VISIBLE
//                refview23.visibility = View.VISIBLE
                lref32.visibility = View.VISIBLE
//                ref3.visibility = View.VISIBLE
                ref1neeche.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                twoLayout = true
            }
        }

        ref3.setOnClickListener {

            if (threeLayout) {
                lrefu23.visibility = View.GONE
//                refviewu24.visibility = View.GONE
                lref232.visibility = View.GONE
//                refviewu243.visibility = View.GONE
                lref233.visibility = View.GONE
//                refviewu2431.visibility = View.GONE
                lref2334.visibility = View.GONE
                ref3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.downs, 0)
                threeLayout = false
            } else {
                lrefu23.visibility = View.VISIBLE
//                refviewu24.visibility = View.VISIBLE
                lref232.visibility = View.VISIBLE
//                refviewu243.visibility = View.VISIBLE
                lref233.visibility = View.VISIBLE
//                refviewu2431.visibility = View.VISIBLE
                lref2334.visibility = View.VISIBLE
                ref3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.ups, 0)
                threeLayout = true

            }

        }

        etAadharNumber.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val aadharNumberVal = etAadharNumber.text.toString()
                if (aadharNumberVal.isNotEmpty() && aadharNumberVal.length != 12) {
                    etAadharNumber.error = "Enter valid aadhar number"
                }
            }
        }
        etMobile.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val alternateNumber = etMobile.text.toString()
                if (alternateNumber.length != 10) {
                    etMobile.error = "Enter valid mobile no"
                }
            }
        }
        val etRefName1 = findViewById<TextView>(R.id.etRefName1)
        etRefName1.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 100)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val etRefName2 = findViewById<TextView>(R.id.etRefName2)
        etRefName2.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 200)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val etRefName23 = findViewById<TextView>(R.id.etRefName23)
        etRefName23.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 300)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val etRefName234 = findViewById<TextView>(R.id.etRefName234)
        etRefName234.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 400)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val etRefName15 = findViewById<TextView>(R.id.etRefName15)
        etRefName15.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 500)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val etRefName156 = findViewById<TextView>(R.id.etRefName156)
        etRefName156.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 600)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        lrefu.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 100)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        lrefu2.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 200)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        lrefu23.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 300)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        lrefu24.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 400)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        lrefu5.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 500)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        lrefu56.setOnClickListener {
            try {
                val pickContactIntent = Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"))
                pickContactIntent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
                startActivityForResult(pickContactIntent, 600)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        etRefMobile1.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile1.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile1.error = "Enter valid mobile no"
                }
            }
        }
        val etRefMobile2 = findViewById<TextView>(R.id.etRefMobile2)

        etRefMobile2.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile2.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile2.error = "Enter valid mobile no"
                }
            }
        }
        val etRefMobile23 = findViewById<TextView>(R.id.etRefMobile23)

        etRefMobile23.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile23.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile23.error = "Enter valid mobile no"
                }
            }
        }
        val etRefMobile156 = findViewById<TextView>(R.id.etRefMobile156)

        etRefMobile156.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile156.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile156.error = "Enter valid mobile no"
                }
            }
        }
        val etRefMobile15 = findViewById<TextView>(R.id.etRefMobile15)

        etRefMobile15.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile15.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile15.error = "Enter valid mobile no"
                }
            }
        }
        val etRefMobile234 = findViewById<TextView>(R.id.etRefMobile234)

        etRefMobile234.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeNumberVal = etRefMobile234.text.toString()
                if (officeNumberVal.length != 10) {
                    etRefMobile234.error = "Enter valid mobile no"
                }
            }
        }
        etAccountNo.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val accountNoVal = etAccountNo.text.toString()
                if (accountNoVal.isEmpty()) {
                    etAccountNo.error = "Account no. is mandatory"
                }
            }
        }
        etIfsc.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val ifscVal = etIfsc.text.toString()
                if (ifscVal.isEmpty()) {
                    etIfsc.error = "IFSC is mandatory"
                }
            }
        }
        officeEmail.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                val officeEmailVal = officeEmail.text.toString()
                if (officeEmailVal.isEmpty()) {
                    officeEmail.error = "Office email is mandatory"
                }
            }
        }

        saveProfileBtn.setOnClickListener {

            if(UtilityMethods.isNetworkAvailable(this)){

                //
                var aadharNumberVal = etAadharNumber.text.toString().trim()
                val accountNoVal = etAccountNo.text.toString()
                val ifscVal = etIfsc.text.toString()
                val mobileVal = etMobile.text.toString()
                val emailVal = etAlternateEmail.text.toString()
                val officeNumberVal = officeNumber.text.toString()
                val officeEmailVal = officeEmail.text.toString()

                val etRefName1 = findViewById<TextView>(R.id.etRefName1)
                val etRefEmail = findViewById<TextView>(R.id.etRefEmail)
                val etRefRelation = findViewById<TextView>(R.id.etRefRelation)


                val etRefName2 = findViewById<TextView>(R.id.etRefName2)
                val etRefEmail2 = findViewById<TextView>(R.id.etRefEmail2)
                val etRefMobile2 = findViewById<TextView>(R.id.etRefMobile2)
                val etRefRelation2 = findViewById<TextView>(R.id.etRefRelation2)


                val etRefName156 = findViewById<TextView>(R.id.etRefName156)
                val etRefEmail56 = findViewById<TextView>(R.id.etRefEmail56)
                val etRefMobile156 = findViewById<TextView>(R.id.etRefMobile156)
                val etRefRelation56 = findViewById<TextView>(R.id.etRefRelation56)


                val etRefRelation5 = findViewById<TextView>(R.id.etRefRelation5)
                val etRefMobile15 = findViewById<TextView>(R.id.etRefMobile15)
                val etRefEmail5 = findViewById<TextView>(R.id.etRefEmail5)
                val etRefName15 = findViewById<TextView>(R.id.etRefName15)


                val etRefRelation234 = findViewById<TextView>(R.id.etRefRelation234)
                val etRefEmail234 = findViewById<TextView>(R.id.etRefEmail234)
                val etRefMobile234 = findViewById<TextView>(R.id.etRefMobile234)
                val etRefName234 = findViewById<TextView>(R.id.etRefName234)


                val etRefRelation23 = findViewById<TextView>(R.id.etRefRelation23)
                val etRefEmail23 = findViewById<TextView>(R.id.etRefEmail23)
                val etRefName23 = findViewById<TextView>(R.id.etRefName23)


                val ref1Name = etRefName1.text.toString()
                val ref1Email = etRefEmail.text.toString()
                val ref1Mobile = etRefMobile1.text.toString()
                val ref1Rel = etRefRelation.text.toString()

                val ref2Name = etRefName2.text.toString()
                val ref2Email = etRefEmail2.text.toString()
                val ref2Mobile = etRefMobile2.text.toString()
                val ref2Rel = etRefRelation2.text.toString()

                val ref3Name = etRefName23.text.toString()
                val ref3Email = etRefEmail23.text.toString()
                val ref3Mobile = etRefMobile23.text.toString()
                val ref3Rel = etRefRelation23.text.toString()

                val ref4Name = etRefName234.text.toString()
                val ref4Email = etRefMobile234.text.toString()
                val ref4Mobile = etRefEmail234.text.toString()
                val ref4Rel = etRefRelation234.text.toString()

                val ref5Name = etRefName15.text.toString()
                val ref5Email = etRefEmail5.text.toString()
                val ref5Mobile = etRefMobile15.text.toString()
                val ref5Rel = etRefRelation5.text.toString()

                val ref6Name = etRefName156.text.toString()
                val ref6Email = etRefEmail56.text.toString()
                val ref6Mobile = etRefMobile156.text.toString()
                val ref6Rel = etRefRelation56.text.toString()



                if (ref1Name.isNotEmpty() &&
                        ref1Mobile.isNotEmpty() && ref1Rel.isNotEmpty()
                        && ref2Name.isNotEmpty()
                        && ref2Mobile.isNotEmpty() && ref2Rel.isNotEmpty() &&
                        ref3Name.isNotEmpty() &&
                        ref3Mobile.isNotEmpty() && ref3Rel.isNotEmpty()) {

                    if (ref1Mobile.length != 10) {
                        UtilityMethods.showToast(this, "Enter valid mobile no")
                        return@setOnClickListener
                    }

                    if (ref2Mobile.length != 10) {
                        UtilityMethods.showToast(this, "Enter valid mobile no")
                        return@setOnClickListener
                    }
                    if (ref3Mobile.length != 10) {
                        UtilityMethods.showToast(this, "Enter valid mobile no")
                        return@setOnClickListener
                    }

                    if (accountNoVal.isNotEmpty() &&
                            ifscVal.isNotEmpty() &&
                            officeEmailVal.isNotEmpty() &&
                            passOutVal.isNotEmpty() &&
                            degreeVal.isNotEmpty() &&
                            mobileVal.isNotEmpty()
                    ) {

                        if (passOutVal != "NONE") {

                            if (degreeVal != "NONE") {


                                if (rentType != RentType.NOTHING) {

                                    if (holidayType != HolidayType.NOTHING) {

                                        if (travelType != TravelType.NOTHING) {

//                                if (aadharNumberVal.length == 12) {

//                        if (mobileVal.length == 10) {

                                            val contactListing = ArrayList<ContactList>()
                                            val contactList1 = ContactList()
                                            val contactList2 = ContactList()
                                            val contactList3 = ContactList()


                                            contactList1.name = ref1Name
                                            contactList1.email = ref1Email
                                            contactList1.relation = ref1Rel
                                            contactList1.mobile = ref1Mobile

                                            contactList2.name = ref2Name
                                            contactList2.email = ref2Email
                                            contactList2.relation = ref2Rel
                                            contactList2.mobile = ref2Mobile

                                            contactList3.name = ref3Name
                                            contactList3.email = ref3Email
                                            contactList3.relation = ref3Rel
                                            contactList3.mobile = ref3Mobile

                                            contactListing.add(contactList1)
                                            contactListing.add(contactList2)
                                            contactListing.add(contactList3)
                                            if (count > 0) {

                                                if (ref4Name.isNotEmpty() && ref4Rel.isNotEmpty() && ref4Mobile.isNotEmpty()) {

                                                    if (ref4Mobile.length != 10) {
                                                        UtilityMethods.showToast(this, "Enter valid mobile no")
                                                        return@setOnClickListener
                                                    }
                                                    val contactList4 = ContactList()
                                                    contactList4.name = ref4Name
                                                    contactList4.email = ref4Email
                                                    contactList4.relation = ref4Rel
                                                    contactList4.mobile = ref4Mobile

                                                    contactListing.add(contactList4)
                                                }
//                                    else{
//                                        UtilityMethods.showToast(this, "Fill all details of Reference 4")
//                                        return@setOnClickListener
//                                    }
                                                if (ref5Name.isNotEmpty() && ref5Rel.isNotEmpty() && ref5Mobile.isNotEmpty()) {
                                                    if (ref5Mobile.length != 10) {
                                                        UtilityMethods.showToast(this, "Enter valid mobile no")
                                                        return@setOnClickListener
                                                    }
                                                    val contactList5 = ContactList()
                                                    contactList5.name = ref5Name
                                                    contactList5.email = ref5Email
                                                    contactList5.relation = ref5Rel
                                                    contactList5.mobile = ref5Mobile
                                                    contactListing.add(contactList5)
                                                }
//                                    else{
//                                        UtilityMethods.showToast(this, "Fill all details of Reference 5")
//                                        return@setOnClickListener
//                                    }
                                                if (ref6Name.isNotEmpty() && ref6Rel.isNotEmpty() && ref6Mobile.isNotEmpty()) {

                                                    if (ref6Mobile.length != 10) {
                                                        UtilityMethods.showToast(this, "Enter valid mobile no")
                                                        return@setOnClickListener
                                                    }
                                                    val contactList6 = ContactList()
                                                    contactList6.name = ref6Name
                                                    contactList6.email = ref6Email
                                                    contactList6.relation = ref6Rel
                                                    contactList6.mobile = ref6Mobile
                                                    contactListing.add(contactList6)
                                                }
//                                    else{
//                                        UtilityMethods.showToast(this, "Fill all details of Reference 6")
//                                        return@setOnClickListener
//                                    }

                                                UtilityMethods.showProgressDialog(this)

                                                if (aadharNumberVal.isNullOrEmpty()) {
                                                    aadharNumberVal = "null"
                                                }
                                                val saveProfile = SaveProfile(aadharBack, aadharFront,
                                                        aadharNumberVal, accountNoVal, emailVal,
                                                        mobileVal, aadharbankStatement,
                                                        degreeVal, holidayType,
                                                        ifscVal, officeEmailVal,
                                                        officeNumberVal, passOutVal, rentType, travelType,
                                                        aadharSalarySlip, contactListing, bankPass.text.toString().trim(), salarySlippass.text.toString().trim())

                                                APIConnector.completeProfile(UserCredentials.getToken(this), saveProfile, this)
                                            } else {
                                                if (aadharNumberVal.isNullOrEmpty()) {
                                                    aadharNumberVal = "null"
                                                }
                                                UtilityMethods.showProgressDialog(this)
                                                val saveProfile = SaveProfile(aadharBack, aadharFront,
                                                        aadharNumberVal, accountNoVal, emailVal,
                                                        mobileVal, aadharbankStatement,
                                                        degreeVal, holidayType,
                                                        ifscVal, officeEmailVal,
                                                        officeNumberVal, passOutVal, rentType, travelType,
                                                        aadharSalarySlip, contactListing, bankPass.text.toString().trim(), salarySlippass.text.toString().trim())


                                                APIConnector.completeProfile(UserCredentials.getToken(this), saveProfile, this)
                                            }

//                        } else {
//                            UtilityMethods.showToast(this, "Enter 10 digit mobile number")
//                        }
//                                } else {
//                                    UtilityMethods.showToast(this, "Enter valid aadhar number")
//                                }

                                        } else {
                                            UtilityMethods.showToast(this, "Please select travel type")
                                        }
                                    } else {
                                        UtilityMethods.showToast(this, "Please select holiday type")
                                    }


                                } else {
                                    UtilityMethods.showToast(this, "Please select rent type")

                                }

                            } else {
                                UtilityMethods.showToast(this, "Please select degree")

                            }
                        } else {
                            UtilityMethods.showToast(this, "Please select passing year")
                        }

                    } else {
                        UtilityMethods.showToast(this, "Fill the mandatory fields")
                    }
                } else {
                    UtilityMethods.showToast(this, "Please add at-least 3 contact references")
                }
                //
            }else{
                UtilityMethods.showToast(this, "Please check your internet connection.")
            }

        }

        etAadharFront.setOnClickListener {
            openFile(200)
            attached = 0

        }
        etAadharBack.setOnClickListener {
            openFile(200)
            attached = 1
        }
        salarySlip.setOnClickListener {
            openFile(200)
            attached = 2

        }
        bankStatement.setOnClickListener {
            openFile(200)
            attached = 3

        }

        passoutSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                if (position > 0) {
                    passOutVal = parent.getItemAtPosition(position).toString()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                passOutVal = "NONE"
            }
        }

        degreeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (position > 0) {
                    degreeVal = parent.getItemAtPosition(position).toString()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                degreeVal = "NONE"
            }
        }

        travelSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                when (position) {
                    0 -> travelType = TravelType.NOTHING
                    1 -> travelType = TravelType.PUBLIC_TRANSPORT
                    2 -> travelType = TravelType.OWN_VEHICLE
                    3 -> travelType = TravelType.WITH_FRIENDS
                    4 -> travelType = TravelType.CAB
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                travelType = TravelType.NOTHING
            }
        }


        monthlyRent.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                when (position) {
                    0 -> rentType = RentType.NOTHING
                    1 -> rentType = RentType.DONT_PAY
                    2 -> rentType = RentType.LESS_THAN_5000
                    3 -> rentType = RentType.BTW_5000_10000
                    4 -> rentType = RentType.BTW_10000_15000
                    5 -> rentType = RentType.BTW_15000_20000
                    6 -> rentType = RentType.MORE_THAN_20000
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                rentType = RentType.NOTHING
            }
        }

        holidaySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                when (position) {
                    0 -> holidayType = HolidayType.NOTHING
                    1 -> holidayType = HolidayType.NOT_MARRIED
                    2 -> holidayType = HolidayType.MOVIE_DINNER
                    3 -> holidayType = HolidayType.AT_HOME
                    4 -> holidayType = HolidayType.WITH_FRIENDS
                    5 -> holidayType = HolidayType.READING_BOOKS_SHOPPING
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                holidayType = HolidayType.NOTHING
            }
        }
    }

    override fun <T> apiSuccess(body: T) {
        UtilityMethods.showToast(this, "Profile Created Successfully...")
        startActivity(Intent(this@ProfileActivity, ApplyLoanActivity::class.java))
        overridePendingTransition(R.anim.enter, R.anim.exit)
        finish()
    }

    override fun apiError(errorDTO: ErrorDto) {
        if (errorDTO.fieldErrors != null) {
            val message = errorDTO.fieldErrors[0].message?.get(0)
            val field = errorDTO.fieldErrors[0].field
            UtilityMethods.showToast(applicationContext, "$field: $message")
        } else {
            UtilityMethods.showToast(applicationContext, errorDTO.description)
        }
    }

    override fun error(body: String?) {
        UtilityMethods.showToast(applicationContext, body!!)
    }

    private fun uploadFile(files: File, userToken: String) {
        try {
            val sizeInBytes = files.length()
            val sizeInMb = sizeInBytes / (1024 * 1024)
            if (files.name.contains("pdf") || files.name.contains("jpg") || files.name.contains("jpeg")) {
                if (sizeInMb <= 2) {
                    val service = ServiceGenerator.createService(FileUploadService::class.java)
                    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), files)
                    val body = MultipartBody.Part.createFormData("file", files.name, requestFile)
                    val call = service.uploadProfilePic(userToken, body)
                    UtilityMethods.showProgressDialog(this)
                    call.enqueue(object : Callback<UploadImageModel> {

                        override fun onResponse(call: Call<UploadImageModel>, response: Response<UploadImageModel>) {
                            UtilityMethods.dismissProgressDialog()
                            if (response.isSuccessful) {
                                try {
                                    Log.i("documentName", response.body().result?.documentName)

                                    when (attached) {
                                        0 -> {
                                            aadharFront = response.body().result?.documentName!!
                                            etAadharFront.text = "Uploaded"
                                            etAadharFront.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.uploaded_tick, 0)
                                        }
                                        1 -> {
                                            aadharBack = response.body().result?.documentName!!
                                            etAadharBack.text = "Uploaded"
                                            etAadharBack.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.uploaded_tick, 0)

                                        }
                                        2 -> {
                                            aadharSalarySlip = response.body().result?.documentName!!
                                            salarySlip.text = "Uploaded"
                                            salarySlip.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.uploaded_tick, 0)

                                        }
                                        3 -> {
                                            aadharbankStatement = response.body().result?.documentName!!
                                            bankStatement.text = "Uploaded"
                                            bankStatement.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.uploaded_tick, 0)

                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            } else {
                                try {
                                    Log.i("Bussh : ", response.errorBody().toString())
                                    UtilityMethods.showToast(this@ProfileActivity, response.errorBody().string())
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }

                            }
                        }

                        override fun onFailure(call: Call<UploadImageModel>, t: Throwable?) {
                            Log.e("Upload errors:", t!!.message)
                            UtilityMethods.dismissProgressDialog()
                            if (t != null) {
                                UtilityMethods.showToast(this@ProfileActivity, t.message!!)
                            }
                        }
                    })
                } else {
                    UtilityMethods.showToast(applicationContext, "File size must be less than 2 MB")
                }
            } else {
                UtilityMethods.showToast(this, "Only pdf, jpg and jpeg is allowed")
            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun openFile(CODE: Int) {
        var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.type = "*/*"
        chooseFile = Intent.createChooser(chooseFile, "Choose a file")
        startActivityForResult(chooseFile, 1000)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1000) {
                if (data == null) {
                    //no data present
                    return
                }
                val selectedFileUri = data.data
                // Log.i("selectedFileUri", selectedFileUri.toString())
                val selection = RealPathUtil.getRealPath(this, selectedFileUri)
                //    Log.i("selectedFile", selection.toString())
                if (selection != null) {
                    uploadFile(File(selection), UserCredentials.getToken(this))
                } else {
                    UtilityMethods.showToast(this@ProfileActivity, "Problem in getting file")
                }
            } else if (requestCode == 100) {
                callContactPicker(data, etRefName1, etRefMobile1)
            } else if (requestCode == 200) {
                callContactPicker(data, etRefName2, etRefMobile2)
            } else if (requestCode == 300) {
                callContactPicker(data, etRefName23, etRefMobile23)
            } else if (requestCode == 400) {
                callContactPicker(data, etRefName234, etRefMobile234)
            } else if (requestCode == 500) {
                callContactPicker(data, etRefName15, etRefMobile15)
            } else if (requestCode == 600) {
                callContactPicker(data, etRefName156, etRefMobile156)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            finish()
            super.onOptionsItemSelected(item)
        } else super.onOptionsItemSelected(item)
    }

    private fun callContactPicker(intent: Intent?, nameField: TextView, contactField: TextView) {
        val contactUri = intent!!.data
        val projection = arrayOf(ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        val cursor = contentResolver
                .query(contactUri!!, projection, null, null, null)
        cursor!!.moveToFirst()
        val column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
        val columns = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        val number = cursor.getString(column)
        val name = cursor.getString(columns)
        Log.i("NAMMAMMAMA", name)
        nameField.text = name
        var contact = number.replace("[-+.^:, )(]".toRegex(), "")
        when {
            contact.length > 10 -> {
                contact = contact.substring(contact.length - 10, contact.length)
                contactField.text = contact
            }
            contact.length == 10 -> contactField.setText(contact)
            else -> UtilityMethods.showToast(this, "Choose 10 digit mobile no")
        }
    }
}
