package com.loanit.app

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SeekBar
import com.loanit.app.net.enum.CityType
import com.loanit.app.net.requestdto.UserContacts
import com.loanit.app.util.AppConstants
import kotlinx.android.synthetic.main.activity_apply_loan_new.*
import kotlinx.android.synthetic.main.activity_eligibility_criteria.*
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {


    var cityType = CityType.NONE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                when (position) {
                    1 -> cityType = CityType.Bangalore
                    2 -> cityType = CityType.Hyderabad
                    3 -> cityType = CityType.Chennai
                    4 -> cityType = CityType.Pune
                    5 -> cityType = CityType.Hyderabad
                    6 -> cityType = CityType.Mumbai
                    7 -> cityType = CityType.Noida
                    8 -> cityType = CityType.Gurgaon
                    9 -> cityType = CityType.Kolkata
                    10 -> cityType = CityType.Bhubaneswar
                    11 -> cityType = CityType.Coimbatore
                    12 -> cityType = CityType.Cochin
                    13 -> cityType = CityType.Vizag
                    14 -> cityType = CityType.Ahmedabad
                    15 -> cityType = CityType.Indore
                    16 -> cityType = CityType.Delhi
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                cityType = CityType.NONE

            }
        }
    }
}

