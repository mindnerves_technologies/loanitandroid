package com.loanit.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loanit.app.R;
import com.loanit.app.net.requestdto.NotificationList;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{

    Context context;
    ArrayList<NotificationList> listOfAllNotification;

    public NotificationAdapter(Context context, ArrayList<NotificationList> listOfAllNotification) {
        this.context = context;
        this.listOfAllNotification = listOfAllNotification;
    }


    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.notification_screen,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int i) {
        holder.txtTitle.setText(listOfAllNotification.get(i).getTitle());
        holder.txtMessage.setText(listOfAllNotification.get(i).getMessage());
        holder.txtDateTime.setText(listOfAllNotification.get(i).getDate());

    }

    @Override
    public int getItemCount() {
        return listOfAllNotification.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle,txtMessage,txtDateTime;
        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle=(TextView)itemView.findViewById(R.id.txtTitle);
            txtMessage=(TextView)itemView.findViewById(R.id.txtMsg);
            txtDateTime=(TextView)itemView.findViewById(R.id.date_time);
        }
    }
}
