package com.loanit.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loanit.app.R;
import com.loanit.app.net.responsedto.EmiResponse;
import com.loanit.app.net.responsedto.LoanStatusResponse;
import com.loanit.app.util.UtilityView;

import java.util.List;

//https://stackoverflow.com/questions/18444227/get-accounts-permission-while-using-gcm-why-is-this-needed
public class LoanStatusAdapter extends RecyclerView.Adapter {
    private List<LoanStatusResponse> list;

    public Context context;

    public LoanStatusAdapter(Context context, List<LoanStatusResponse> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_loan_status, parent, false);

        vh = new NewsViewHolder(v);

        return vh;
    }

    @Override

    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NewsViewHolder) {

            /*if (!list.get(position).getMembershipType().equals("Basic")) {
                if(list.get(position).getExhaustedDays()>33){
                    ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.GONE);
                }else {
                    ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.VISIBLE);
                }
            }else {
                ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.VISIBLE);
                ((NewsViewHolder) holder).emiCheck.setVisibility(View.GONE);
            }*/



            if(list.get(position).getApprovalDate()!=null && !list.get(position).getApprovalDate().isEmpty()){
                ((NewsViewHolder) holder).loanApproveDateVal.setText(list.get(position).getApprovalDate());
            }else {
                ((NewsViewHolder) holder).layApprovedDate.setVisibility(View.GONE);
            }


            ((NewsViewHolder) holder).loanDateVal.setText(list.get(position).getLoanDate());
            ((NewsViewHolder) holder).loanIdVal.setText("LTL" + list.get(position).getLoanId());

           /* if (list.get(position).getExhaustedDays().equalsIgnoreCase("0")) {
                ((NewsViewHolder) holder).daysVal.setText("" + list.get(position).getExhaustedDays());
            } else {
                ((NewsViewHolder) holder).daysVal.setText(""+list.get(position).getExhaustedDays().substring(1, list.get(position).getExhaustedDays().length()));
            }*/
            ((NewsViewHolder) holder).daysVal.setText("" + list.get(position).getExhaustedDays());

            ((NewsViewHolder) holder).layEmi1.setVisibility(View.GONE);
            ((NewsViewHolder) holder).layEmi2.setVisibility(View.GONE);
            ((NewsViewHolder) holder).layEmi3.setVisibility(View.GONE);
            UtilityView.collapse(((NewsViewHolder) holder).layEmi);

            if(list.get(position).getLoanEmiUser().size()>0){
                //
                if(list.get(position).getExhaustedDays()>33){
                    UtilityView.expand(((NewsViewHolder) holder).layEmi);
                    ((NewsViewHolder) holder).emiCheck.setVisibility(View.GONE);
                    ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.GONE);
                }else {
                    ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.VISIBLE);
                }
                //
                for(int i=0 ; i<list.get(position).getLoanEmiUser().size();i++){
                    EmiResponse item = list.get(position).getLoanEmiUser().get(i);

                    if(item.getEmiStatus().name().equals("CLEARED")){
                        UtilityView.expand(((NewsViewHolder) holder).layEmi);
                        ((NewsViewHolder) holder).emiCheck.setVisibility(View.GONE);
                        ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.GONE);
                    }

                    if(i==0){
                        ((NewsViewHolder) holder).layEmi1.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount1,((NewsViewHolder) holder).emiDueDate1,((NewsViewHolder) holder).emiStatus1);
                    }else if(i==1){
                        ((NewsViewHolder) holder).layEmi2.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount2,((NewsViewHolder) holder).emiDueDate2,((NewsViewHolder) holder).emiStatus2);
                    }else if(i==2){
                        ((NewsViewHolder) holder).layEmi3.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount3,((NewsViewHolder) holder).emiDueDate3,((NewsViewHolder) holder).emiStatus3);
                    }

                }
            }else {
                ((NewsViewHolder) holder).layPayableAmount.setVisibility(View.VISIBLE);
                ((NewsViewHolder) holder).emiCheck.setVisibility(View.GONE);
            }

            switch (list.get(position).getLoanStatus().name()) {
                case "APPROVED":
                    //((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.active_button));
                    ((NewsViewHolder) holder).loanStatusVal.setTextColor(context.getResources().getColor(R.color.green));
                    break;
                case "CLEARED":
                    //((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.cleared_button));
                    ((NewsViewHolder) holder).loanStatusVal.setTextColor(context.getResources().getColor(R.color.darkBlue));
                    break;
                case "CANCELLED":
                    //((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.cancelled_button));
                    ((NewsViewHolder) holder).loanStatusVal.setTextColor(context.getResources().getColor(R.color.cancel));
                    break;
                default:
                    //((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.in_process_button));
                    ((NewsViewHolder) holder).loanStatusVal.setTextColor(context.getResources().getColor(R.color.in_progress));

            }
            ((NewsViewHolder) holder).loanStatusVal.setText(list.get(position).getLoanStatus().name());

            ((NewsViewHolder) holder).payableVal.setText(""+ list.get(position).getTotalAmount());
            ((NewsViewHolder) holder).newsData = list.get(position);




            ((NewsViewHolder) holder).emiCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if(isChecked){
                        UtilityView.expand(((NewsViewHolder) holder).layEmi);

                    }else{
                        UtilityView.collapse(((NewsViewHolder) holder).layEmi);

                    }
                }
            });
        }
    }

    private void setEmiDetails(EmiResponse item, TextView emiAmount, TextView emiDueDate, TextView emiStatus) {
        switch (item.getEmiStatus().name()) {
            case "APPROVED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.active_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.green));
                break;
            case "CLEARED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.cleared_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.darkBlue));
                break;
            case "CANCELLED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.cancelled_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            default:
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.in_process_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.in_progress));

        }
        emiStatus.setText(item.getEmiStatus().name());
        emiAmount.setText(""+item.getTotalEmiAmount());
        emiDueDate.setText(item.getEmiDueDate());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        protected TextView daysVal;
        protected TextView loanDateVal;
        protected TextView loanIdVal;
        protected TextView loanStatusVal;
        protected TextView payableVal;
        protected LoanStatusResponse newsData;

        protected CheckBox emiCheck;
        protected RelativeLayout layHeading,layEmi1,layEmi2,layEmi3;

        protected TextView emiAmount1,emiAmount2,emiAmount3;
        protected TextView emiDueDate1,emiDueDate2,emiDueDate3;
        protected TextView emiStatus1,emiStatus2,emiStatus3;

        protected RelativeLayout layApprovedDate,layPayableAmount,layEmi;
        protected TextView loanApproveDateVal;

        public NewsViewHolder(View view) {
            super(view);

            this.layEmi = view.findViewById(R.id.layEmi);

            this.layPayableAmount = view.findViewById(R.id.layPayableAmount);
            this.layApprovedDate = view.findViewById(R.id.layApprovedDate);
            this.loanApproveDateVal = view.findViewById(R.id.loanApproveDateVal);

            this.loanDateVal = view.findViewById(R.id.loanDateVal);
            this.loanIdVal = view.findViewById(R.id.loanIdVal);
            this.loanStatusVal = view.findViewById(R.id.loanStatusVal);
            this.payableVal = view.findViewById(R.id.payableVal);
            this.daysVal = view.findViewById(R.id.exhaustedDaysVal);

            this.emiCheck = view.findViewById(R.id.emiCheck);

            this.layHeading = view.findViewById(R.id.lay_premium_heading);
            this.layEmi1 = view.findViewById(R.id.lay_premium_emi_1);
            this.layEmi2 = view.findViewById(R.id.lay_premium_emi_2);
            this.layEmi3 = view.findViewById(R.id.lay_premium_emi_3);

            this.emiAmount1 = view.findViewById(R.id.tv_amount_emi_1);
            this.emiDueDate1 = view.findViewById(R.id.tv_date_emi_1);
            this.emiStatus1 = view.findViewById(R.id.tv_status_emi_1);

            this.emiAmount2 = view.findViewById(R.id.tv_amount_emi_2);
            this.emiDueDate2 = view.findViewById(R.id.tv_date_emi_2);
            this.emiStatus2 = view.findViewById(R.id.tv_status_emi_2);

            this.emiAmount3 = view.findViewById(R.id.tv_amount_emi_3);
            this.emiDueDate3 = view.findViewById(R.id.tv_date_emi_3);
            this.emiStatus3 = view.findViewById(R.id.tv_status_emi_3);

        }
    }


}