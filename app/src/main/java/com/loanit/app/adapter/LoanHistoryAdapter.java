package com.loanit.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.loanit.app.R;
import com.loanit.app.net.responsedto.EmiResponse;
import com.loanit.app.net.responsedto.LoanHistoryResponse;

import java.util.List;


public class LoanHistoryAdapter extends RecyclerView.Adapter {
    private List<LoanHistoryResponse> list;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    public Context context;

    public LoanHistoryAdapter(Context context, List<LoanHistoryResponse> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_loan_history, parent, false);
        vh = new NewsViewHolder(v);
//        if (viewType == VIEW_TYPE_ITEM) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loan_history, parent, false);
//            return new NewsViewHolder(view);
//        } else {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_loader, parent, false);
//            return new LoadingViewHolder(view);
//        }
        return vh;
    }

    @Override

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NewsViewHolder) {
            //((NewsViewHolder) holder).loanDateVal.setText(list.get(position).getRepaidDate());
            ((NewsViewHolder) holder).loanIdVal.setText("LTL" + list.get(position).getLoanId());

            if(list.get(position).getLoanEmiUser().size()>0){

                ((NewsViewHolder) holder).tvPreClosed.setVisibility(View.GONE);
                ((NewsViewHolder) holder).scrollViewPreClosed.setVisibility(View.GONE);
                ((NewsViewHolder) holder).scrollViewEmi.setVisibility(View.VISIBLE);

                ((NewsViewHolder) holder).emiAmount1.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiAmount2.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiAmount3.setVisibility(View.GONE);

                ((NewsViewHolder) holder).emiRepaidDate1.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiRepaidDate2.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiRepaidDate3.setVisibility(View.GONE);

                ((NewsViewHolder) holder).emiStatus1.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiStatus2.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiStatus3.setVisibility(View.GONE);

                ((NewsViewHolder) holder).emiCredit1.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiCredit2.setVisibility(View.GONE);
                ((NewsViewHolder) holder).emiCredit3.setVisibility(View.GONE);

                for(int i=0 ; i<list.get(position).getLoanEmiUser().size();i++){
                    EmiResponse item = list.get(position).getLoanEmiUser().get(i);
                    if(i==0){
                        ((NewsViewHolder) holder).emiAmount1.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiRepaidDate1.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiStatus1.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiCredit1.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount1,((NewsViewHolder) holder).emiRepaidDate1,((NewsViewHolder) holder).emiStatus1,((NewsViewHolder) holder).emiCredit1);
                    }else if(i==1){
                        ((NewsViewHolder) holder).emiAmount2.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiRepaidDate2.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiStatus2.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiCredit2.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount2,((NewsViewHolder) holder).emiRepaidDate2,((NewsViewHolder) holder).emiStatus2,((NewsViewHolder) holder).emiCredit2);
                    }else if(i==2){
                        ((NewsViewHolder) holder).emiAmount3.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiRepaidDate3.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiStatus3.setVisibility(View.VISIBLE);
                        ((NewsViewHolder) holder).emiCredit3.setVisibility(View.VISIBLE);
                        setEmiDetails(item, ((NewsViewHolder) holder).emiAmount3,((NewsViewHolder) holder).emiRepaidDate3,((NewsViewHolder) holder).emiStatus3,((NewsViewHolder) holder).emiCredit3);
                    }

                }

            }else {

                if(!list.get(position).getLoanPreClosed() || list.get(position).getLoanStatus().name().equals("CANCELLED")){
                    ((NewsViewHolder) holder).tvPreClosed.setVisibility(View.GONE);
                }else {
                    ((NewsViewHolder) holder).tvPreClosed.setVisibility(View.VISIBLE);
                }


                ((NewsViewHolder) holder).scrollViewPreClosed.setVisibility(View.VISIBLE);
                ((NewsViewHolder) holder).scrollViewEmi.setVisibility(View.GONE);


                switch (list.get(position).getLoanStatus().name()) {
                    case "APPROVED":
                        //((NewsViewHolder) holder).emiStatusPre.setBackground(context.getResources().getDrawable(R.drawable.active_button));
                        ((NewsViewHolder) holder).emiStatusPre.setTextColor(context.getResources().getColor(R.color.green));
                        break;
                    case "CLEARED":
                        //((NewsViewHolder) holder).emiStatusPre.setBackground(context.getResources().getDrawable(R.drawable.cleared_button));
                        ((NewsViewHolder) holder).emiStatusPre.setTextColor(context.getResources().getColor(R.color.darkBlue));
                        break;
                    case "CANCELLED":
                        //((NewsViewHolder) holder).emiStatusPre.setBackground(context.getResources().getDrawable(R.drawable.cancelled_button));
                        ((NewsViewHolder) holder).emiStatusPre.setTextColor(context.getResources().getColor(R.color.cancel));
                        break;
                    default:
                        //((NewsViewHolder) holder).emiStatusPre.setBackground(context.getResources().getDrawable(R.drawable.in_process_button));
                        ((NewsViewHolder) holder).emiStatusPre.setTextColor(context.getResources().getColor(R.color.in_progress));
                }

                ((NewsViewHolder) holder).emiStatusPre.setText(list.get(position).getLoanStatus().name());
                ((NewsViewHolder) holder).emiAmountPre.setText(""+list.get(position).getTotalAmount());
                ((NewsViewHolder) holder).emiRepaidDatePre.setText(list.get(position).getRepaidDate());
                ((NewsViewHolder) holder).emiCreditPre.setText(""+list.get(position).getTotalCreditPoint());
            }
            /*switch (list.get(position).getLoanStatus().name()) {
                case "APPROVED":
                    ((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.active_button));
                    break;
                case "CLEARED":
                    ((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.cleared_button));
                    break;
                case "CANCELLED":
                    ((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.cancelled_button));
                    break;
                default:
                    ((NewsViewHolder) holder).loanStatusVal.setBackground(context.getResources().getDrawable(R.drawable.in_process_button));

            }
            ((NewsViewHolder) holder).loanStatusVal.setText(list.get(position).getLoanStatus().name());
            ((NewsViewHolder) holder).totalPaidVal.setText("" + list.get(position).getTotalAmount());*/
            //((NewsViewHolder) holder).newsData = list.get(position);
        }
//
// else if (holder instanceof LoadingViewHolder) {
//            showLoadingView((LoadingViewHolder) holder, position);
//        }
    }

    private void setEmiDetails(EmiResponse item, TextView emiAmount, TextView emiDueDate, TextView emiStatus, TextView emiCredit) {
        switch (item.getEmiStatus().name()) {
            case "APPROVED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.active_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.green));
                break;
            case "CLEARED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.cleared_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.darkBlue));
                break;
            case "CANCELLED":
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.cancelled_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.cancel));
                break;
            default:
                //emiStatus.setBackground(context.getResources().getDrawable(R.drawable.in_process_button));
                emiStatus.setTextColor(context.getResources().getColor(R.color.in_progress));
        }
        emiStatus.setText(item.getEmiStatus().name());
        emiAmount.setText(""+item.getTotalEmiAmount());
        emiDueDate.setText(item.getEmiRepaidDate());
        emiCredit.setText(""+item.getEmiCreditPoint());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

//    @Override
//    public int getItemViewType(int position) {
//        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        //protected TextView loanDateVal;
        protected TextView loanIdVal;
        //protected TextView loanStatusVal;
        //protected TextView totalPaidVal;
        //protected LoanHistoryResponse newsData;
        protected TextView emiAmountPre,emiAmount1,emiAmount2,emiAmount3;
        protected TextView emiRepaidDatePre,emiRepaidDate1,emiRepaidDate2,emiRepaidDate3;
        protected TextView emiStatusPre,emiStatus1,emiStatus2,emiStatus3;
        protected TextView emiCreditPre,emiCredit1,emiCredit2,emiCredit3;

        protected HorizontalScrollView scrollViewEmi,scrollViewPreClosed;

        protected TextView tvPreClosed;


        public NewsViewHolder(View view) {
            super(view);
            //this.loanDateVal = view.findViewById(R.id.loanDateVal);
            this.loanIdVal = view.findViewById(R.id.loanIdVal);
            //this.loanStatusVal = view.findViewById(R.id.statusVal);
            //this.totalPaidVal = view.findViewById(R.id.totalPaidVal);

            this.emiAmount1 = view.findViewById(R.id.amount_emi_1);
            this.emiRepaidDate1 = view.findViewById(R.id.repaid_date_emi_1);
            this.emiStatus1 = view.findViewById(R.id.status_emi_1);
            this.emiCredit1 = view.findViewById(R.id.credit_emi_1);

            this.emiAmount2 = view.findViewById(R.id.amount_emi_2);
            this.emiRepaidDate2 = view.findViewById(R.id.repaid_date_emi_2);
            this.emiStatus2 = view.findViewById(R.id.status_emi_2);
            this.emiCredit2 = view.findViewById(R.id.credit_emi_2);

            this.emiAmount3 = view.findViewById(R.id.amount_emi_3);
            this.emiRepaidDate3 = view.findViewById(R.id.repaid_date_3);
            this.emiStatus3 = view.findViewById(R.id.status_emi_3);
            this.emiCredit3 = view.findViewById(R.id.credit_emi_3);

            this.emiAmountPre = view.findViewById(R.id.pre_closed_amount_emi_1);
            this.emiRepaidDatePre = view.findViewById(R.id.pre_closed_repaid_date_emi_1);
            this.emiStatusPre = view.findViewById(R.id.pre_closed_status_emi_1);
            this.emiCreditPre = view.findViewById(R.id.pre_closed_credit_emi_1);

            this.tvPreClosed = view.findViewById(R.id.tvPreClosed);

            this.scrollViewEmi = view.findViewById(R.id.scrollViewEmi);
            this.scrollViewPreClosed = view.findViewById(R.id.scrollViewPreClosed);

        }
    }

//    private class LoadingViewHolder extends RecyclerView.ViewHolder {
//
//        ProgressBar progressBar;
//
//        public LoadingViewHolder(@NonNull View itemView) {
//            super(itemView);
//            progressBar = itemView.findViewById(R.id.progressBar);
//        }
//    }

//    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
//        //ProgressBar would be displayed
//
//    }
}